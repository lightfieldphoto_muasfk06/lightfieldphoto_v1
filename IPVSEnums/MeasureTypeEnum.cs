﻿
////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Enumeration for setting the measurement type
    /// </summary>
    public enum MeasureTypeEnum : int
    {
        /// <summary>
        /// The none flag
        /// </summary>
        None,
        ///// <summary>
        ///// Sets the Circle flag - measuring with a circle shape.
        ///// </summary>
        //Circle,
        /// <summary>
        /// Sets the Ellipse flag - measuring with a ellipse shape.
        /// </summary>
        Ellipse,
        /// <summary>
        /// Sets the Line flag - measuring with a line.
        /// </summary>
        Line,
        /// <summary>
        /// Sets the Rectangle flag - measuring with a rectangle shape.
        /// </summary>
        Rectangle
    }

}