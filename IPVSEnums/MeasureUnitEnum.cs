﻿
////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Enumeration for setting the measurement unit
    /// </summary>
    public enum MeasureUnitEnum : int
    {
        /// <summary>
        /// Sets the pixel unit flag
        /// </summary>
        Pixel,

        /// <summary>
        /// Sets the metric unit flag
        /// </summary>
        Metric,
        None
    }

}


