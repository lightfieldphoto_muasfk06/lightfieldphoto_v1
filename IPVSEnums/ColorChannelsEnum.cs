﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Enumeration for setting the color channel
    /// </summary>
    public enum ColorChannelsEnum : int
    {
        /// <summary>
        /// The RGB flag -> all three color channels are selected
        /// </summary>
        RGB,
        /// <summary>
        /// The blue color channel flag
        /// </summary>
        Blue,
        /// <summary>
        /// The green color channel flag
        /// </summary>
        Green,
        /// <summary>
        /// The red color channel flag
        /// </summary>
        Red,
        /// <summary>
        /// The alpha color channel flag
        /// </summary>
        Alpha,
        /// <summary>
        /// The blue_ green color channels flag
        /// </summary>
        Blue_Green,
        /// <summary>
        /// The blue_ red color channels flag
        /// </summary>
        Blue_Red,
        /// <summary>
        /// The green_ red color channels flag
        /// </summary>
        Green_Red,
        /// <summary>
        /// The gray color channel flag
        /// </summary>
        Gray,
        None
    }

}