﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{

    /// <summary>
    /// Enumeration that is representing pixel bit types and depths
    /// </summary>
    public enum PixelChannelTypeEnum : int
    {
        UnDefined = 0,

        /// <summary>
        /// The 8 Bit = 1 byte unsigned integer pixel values flag
        /// </summary>
        Pixel_U8 = 8,
        /// <summary>
        /// The 8 Bit = 1 byte signed integer pixel values flag
        /// </summary>
        Pixel_S8 = int.MinValue + 8,
        /// <summary>
        /// The 16 Bit = 2 bytes unsigned integer pixel values flag
        /// </summary>
        Pixel_U16 = 16,
        /// <summary>
        /// The 16 Bit = 2 bytes signed integer pixel values flag
        /// </summary>
        Pixel_S16 = int.MinValue + 16,
        /// <summary>
        /// The 32 Bit = 4 bytes unsigned integer pixel values flag
        /// </summary>
        Pixel_U32 = 132,
        /// <summary>
        /// The 32 Bit = 4 bytes signed integer pixel values flag
        /// </summary>
        Pixel_S32 = int.MinValue + 32,
        /// <summary>
        /// The 32 Bit = 4 bytes floating point pixel values flag
        /// </summary>
        Pixel_F32 = 32,
        /// <summary>
        /// The 64 Bit = 8 bytes unsigned integer pixel values flag
        /// </summary>
        Pixel_U64 = 164,
        /// <summary>
        /// The 64 Bit = 8 bytes signed integer pixel values flag
        /// </summary>
        Pixel_S64 = int.MinValue + 64,
        /// <summary>
        /// The 64 Bit = 8 bytes floating point pixel values flag
        /// </summary>
        Pixel_F64 = 64,
        /// <summary>
        /// The 2*32 Bit complex (real + i*imaginary) pixel values flag
        /// </summary>
        Pixel_C64 = 264,
        /// <summary>
        /// The 2*64 Bit complex (real + i*imaginary) pixel values flag
        /// </summary>
        Pixel_C128 = 128,
    }

}