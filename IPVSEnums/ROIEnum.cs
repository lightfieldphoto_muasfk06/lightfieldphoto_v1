﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Enumeration for the 'Region of Interest' = ROI. 
    /// </summary>
    public enum ROITypeEnum : int
    {
        /// <summary>
        /// A rectangle ROI
        /// </summary>
        Rectangle,
        /// <summary>
        /// A circle ROI
        /// </summary>
        Circle,
        /// <summary>
        /// A ellipse ROI
        /// </summary>
        Ellipse,
        /// <summary>
        /// A user defined ROI
        /// </summary>
        UserDefine,
        None
    };

}