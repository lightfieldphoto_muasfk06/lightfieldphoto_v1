﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Message type enumeration for output messages. Through this enumerator the output console distinguishes
    /// the color of text the message should be displayed.
    /// </summary>
    public enum OutputMsgEnum : int 
    {
        /// <summary>
        /// The action enumerator -> blue color
        /// </summary>
        Action,
        /// <summary>
        /// The Parameters enumerator -> operation parameters -> white color
        /// </summary>
        Parameter,
        /// <summary>
        /// The operation enumerator -> white color
        /// </summary>
        Operation,
        /// <summary>
        /// The Error enumerator -> red color
        /// </summary>
        Error,
        /// <summary>
        /// The operation enumerator -> purple color
        /// </Debug>
        Debug,
        /// <summary>
        /// The Abort enumerator -> green color
        /// </summary>
        Abort,
        Default,
    };

}
