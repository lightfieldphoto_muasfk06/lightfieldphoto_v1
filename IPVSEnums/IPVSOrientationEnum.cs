﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Orientation enumeration.
    /// </summary>
    public enum IPVSOrientationEnum : int
    {
        /// <summary>
        /// The horizontal flag
        /// </summary>
        Horizontal = 0,
        /// <summary>
        /// The vertical flag
        /// </summary>
        Vertical = 1,
        None = 2
    };

}