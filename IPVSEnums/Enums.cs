﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Class containing a <see cref="StringCollection"/> with all enumerators available in this class
    /// </summary>
    public static class EnumeratorList
    {
        /// <summary>
        /// The assembly
        /// </summary>
        private static Assembly assembly;
        /// <summary>
        /// The enumerators <see cref="StringCollection"/>
        /// </summary>
        public static StringCollection Enumerators;

        /// <summary>
        /// Initializes the <see cref="EnumeratorList" /> class.
        /// </summary>
        static EnumeratorList()
        {
            assembly = Assembly.GetCallingAssembly();
            Type[] typesArray = assembly.GetTypes();
            Enumerators = new StringCollection();

            foreach (Type t in typesArray)
                Enumerators.Add(t.Name);
        }


    }
}
