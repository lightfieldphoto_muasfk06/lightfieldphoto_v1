﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;

namespace IPVS.IPVSEnumerations
{
    /// <summary>
    /// Metric units enumeration for operations with metric measurements and units
    /// </summary>
    public enum MetricUnitsEnum : int
    {
        /// <summary>
        /// The nm flag -> int value = 0
        /// </summary>
        nm = 0,
        /// <summary>
        /// No metric unit
        /// </summary>
        None = 1,
        /// <summary>
        /// The um flag -> int value = 3
        /// </summary>
        um = 3,
        /// <summary>
        /// The mm flag -> int value = 6
        /// </summary>
        mm = 6,
        /// <summary>
        /// The cm flag -> int value = 7
        /// </summary>
        cm = 7,
        /// <summary>
        /// The dm flag -> int value = 8
        /// </summary>
        dm = 8,
        /// <summary>
        /// The m flag -> int value = 9
        /// </summary>
        m = 9,
        /// <summary>
        /// The km flag -> int value = 9
        /// </summary>
        km = 12
    };
}