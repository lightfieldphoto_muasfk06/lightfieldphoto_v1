﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPVS.IPVSEnumerations
{

    /// <summary>
    /// Enumeration for the output messaging system.
    /// </summary>
    public enum OperationStatusEnum : int
    {
        /// <summary>
        /// Message type -> successful operation
        /// </summary>
        Success,
        /// <summary>
        /// Message type -> still in progress/operation
        /// </summary>
        Operating,
        /// <summary>
        /// Message type -> Erroneous operation
        /// </summary>
        Error,
        /// <summary>
        /// Message type -> Default operation
        /// </summary>
        Default,
    };

}