﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UAM.InformatiX.Text.Json;
using UAM.Optics.LightField.Lytro.IO;
using UAM.Optics.LightField.Lytro.Metadata;
using FastColoredTextBoxNS;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace LightFieldPhoto_V1
{
    public partial class LytroPackageViewerForm : Form
    {

        #region fields

        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.SaveFileDialog extractfileDialog;
        private System.Windows.Forms.FolderBrowserDialog extractToFolder;
        private System.String filePath;
        private System.String extractfilePath;
        public LightFieldPackage Package;
        public System.Drawing.Bitmap[] BitmapsStack;
        public System.Drawing.Bitmap DepthLUT;
        private int actualIndex;
        private int numberOfImgs;
        public IList<LytroImageJSONData> ImgJSONDataSet;
        private string filename;
        string JSONText;

        #endregion fields


        #region constructor
        public LytroPackageViewerForm()
        {
            InitializeComponent();
            fileDialog = new OpenFileDialog();
            extractToFolder = new FolderBrowserDialog();
            ActualIndex = 0;
        }
        #endregion constructor


        #region properties 

        public int ActualIndex
        {
            get
            {
                return actualIndex;
            }
            set
            {
                if (value < numberOfImgs && value >= 0)
                {
                    actualIndex = value;
                }
            }
        }

        #endregion properties


        #region event handler methods

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // set dialog title
            this.fileDialog.Title = "Open Lytro package file";

            // set startdirectory
            this.fileDialog.InitialDirectory = ".";

            this.fileDialog.Filter = "Lytro package file (*.lfp) | *.lfp"
                                                    + "| All files (*.*)|*.*";
            this.fileDialog.FilterIndex = 1;

            // no multiple file selection
            this.fileDialog.Multiselect = false;

            // save directory for the next use of the openfiledialog
            this.fileDialog.RestoreDirectory = true;

            if (this.fileDialog.ShowDialog() == DialogResult.OK)
                if (File.Exists(this.fileDialog.FileName))
            {

                filePath = this.fileDialog.FileName;
                this.filePathStatusLabel.Text = filePath;
                this.filePathStatusLabel.ToolTipText = filePath;
                this.filename = System.IO.Path.GetFileNameWithoutExtension(filePath);
                Package = new LightFieldPackage(File.OpenRead(filePath));

                if (Package != null)
                {
                    if (Package.ComponentsCount > 3)
                    {
                        this.propertyGridPackage.SelectedObject = Package;

                        if (BitmapsStack != null)
                        {
                            if (BitmapsStack.Length > 0)
                            {
                                foreach (Bitmap bmp in BitmapsStack)
                                {
                                    bmp.Dispose();
                                }
                                //for (int i = 0; i < BitmapsStack.Length; i++)
                                //{
                                    //BitmapsStack[i].Dispose();
                                //}
                            }
                            BitmapsStack = null;
                        }

                        numberOfImgs = Package.ComponentsCount - 3;

                        BitmapsStack = new Bitmap[numberOfImgs];
                        ActualIndex = 0;

                        for (int i = 3; i < Package.ComponentsCount; i++)
                        {
                            using (MemoryStream ms = new MemoryStream(Package.Components[i].Data))
                            {
                                BitmapsStack[i - 3] = (Bitmap)Bitmap.FromStream(ms);
                            }
                        }

                        this.imageBox.Image = BitmapsStack[actualIndex];

                        JSONText = Encoding.UTF8.GetString(Package.Components[1].Data);
                        this.FCTextBoxJSON.Text = JSONText;

                        JObject testJObject = JObject.Parse(JSONText);

                        IList<JToken> results = testJObject["picture"]["accelerationArray"][0]["vendorContent"]["imageArray"].Children().ToList();

                        string JSONString2 = results[0].ToString();
                        richTextBoxTest.Text = JSONString2;

                        // serialize JSON results into .NET objects
                        if (ImgJSONDataSet != null)
                        {
                            ImgJSONDataSet.Clear();
                            ImgJSONDataSet = null;
                        }
                        ImgJSONDataSet = new List<LytroImageJSONData>();
                        foreach (JToken result in results)
                        {
                            LytroImageJSONData ImgJSONData = JsonConvert.DeserializeObject<LytroImageJSONData>(result.ToString());
                            ImgJSONDataSet.Add(ImgJSONData);
                        }

                        propertyGridJSON.SelectedObject = ImgJSONDataSet[actualIndex];
                    }
                }
            }

        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.imageBox.ClearImage();
            this.FCTextBoxJSON.Clear();
            if (BitmapsStack != null)
                if (BitmapsStack.Length > 0)
                {
                    foreach (Bitmap bmp in BitmapsStack)
                    {
                        bmp.Dispose();
                    }
                    //for (int i = 0; i < BitmapsStack.Length; i++)
                    //{
                    //BitmapsStack[i].Dispose();
                    //}
                }
            this.propertyGridPackage.SelectedObject = null;
            this.propertyGridPackage.Update();
            this.filePathStatusLabel.Text = "";
        }

        private void tsbSplitPackage_Click(object sender, EventArgs e)
        {
            if (this.Package != null)
            {
                // set dialog title
                this.extractToFolder.Description = "Extract Lytro Package Data";
                // set startdirectory
                this.extractToFolder.RootFolder = Environment.SpecialFolder.Desktop;
                this.extractToFolder.ShowNewFolderButton = true;
                if (this.extractToFolder.ShowDialog() == DialogResult.OK)
                {
                    extractfilePath = this.extractToFolder.SelectedPath;
                    for (int i = 3; i < Package.ComponentsCount; i++)
                        File.WriteAllBytes(extractfilePath + "\\" + this.filename + "_" + i, Package.Components[i].Data);
                }
            }
        }

        private void extractImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Package != null)
            {
                // set dialog title
                this.extractToFolder.Description = "Extract Lytro Package Data";
                // set startdirectory
                this.extractToFolder.RootFolder = Environment.SpecialFolder.Desktop;
                this.extractToFolder.ShowNewFolderButton = true;
                if (this.extractToFolder.ShowDialog() == DialogResult.OK)
                {
                    extractfilePath = this.extractToFolder.SelectedPath;
                    for (int i = 3; i < Package.ComponentsCount; i++)
                        File.WriteAllBytes(extractfilePath + "\\" + this.filename + i + ".jpg", Package.Components[i].Data);
                }
            }
        }


        private void extractJSONToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Package != null)
            {
                // set dialog title
                this.extractToFolder.Description = "Extract Lytro Package Data";
                // set startdirectory
                this.extractToFolder.RootFolder = Environment.SpecialFolder.Desktop;
                this.extractToFolder.ShowNewFolderButton = true;
                if (this.extractToFolder.ShowDialog() == DialogResult.OK)
                {
                    extractfilePath = this.extractToFolder.SelectedPath;
                    System.IO.File.WriteAllText(extractfilePath + "\\" + this.filename + ".JSON", JSONText);
                }
            }
        }

        private void stcImageBoxTabControlPackage_MouseEnter(object sender, EventArgs e)
        {
            stcImageBoxTabControlPackage.BackColor = System.Drawing.Color.FromName("GradientInactiveCaption");
        }

        private void stcImageBoxTabControlPackage_MouseLeave(object sender, EventArgs e)
        {
            stcImageBoxTabControlPackage.BackColor = System.Drawing.Color.FromName("Control");
        }

        private void previousImageStackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Package != null)
            {
                if (BitmapsStack != null)
                    if (BitmapsStack.Length > 0)
                    {
                        this.ActualIndex--;
                        this.propertyGridJSON.SelectedObject = this.ImgJSONDataSet[ActualIndex];
                        this.imageBox.Image = BitmapsStack[ActualIndex];
                    }
            }
        }

        private void nextImageStackToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.Package != null)
            {
                if (BitmapsStack != null)
                    if (BitmapsStack.Length > 0)
                    {
                        this.ActualIndex++;
                        this.propertyGridJSON.SelectedObject = this.ImgJSONDataSet[ActualIndex];
                        this.imageBox.Image = BitmapsStack[ActualIndex];
                    }
            }
        }


        private void tsbOpenPackage_Click(object sender, EventArgs e)
        {
            openFileToolStripMenuItem.PerformClick();
        }


        private void tsbExtractImages_Click(object sender, EventArgs e)
        {
            extractImagesToolStripMenuItem.PerformClick();
        }

        private void tsbPreviousImage_Click(object sender, EventArgs e)
        {
            previousImageStackToolStripMenuItem.PerformClick();
        }

        private void tsbNextImage_Click(object sender, EventArgs e)
        {
            nextImageStackToolStripMenuItem1.PerformClick();
        }

        private void tsbReadJSON_Click(object sender, EventArgs e)
        {
            extractJSONToolStripMenuItem.PerformClick();
        }

        #endregion event handler methods

    }
}
