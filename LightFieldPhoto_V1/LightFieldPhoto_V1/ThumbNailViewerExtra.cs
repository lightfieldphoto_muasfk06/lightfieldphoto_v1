﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using IPVS.IPVSBaseClasses;

namespace IPVS.IPVSControls
{

    /// <summary>
    /// The ThumbNailViewerExtra control, inheriting from <seealso cref="UserControl"/>,
    /// is a control that is hosting <see cref="IPVS.IPVSControls.ThumbControl"/> controls,
    /// that show thumbnails off the project images.
    /// </summary>
    public partial class ThumbNailViewerExtra : UserControl
    {

        #region fields

        private ThumbControl thumbControlSelected;

        // Each picture box will be a square of dimension X pixels
        private System.Drawing.Size dimension;

        // The space between the images and the top, left, and right sides
        private int border = 5;

        // The space between each image.
        private int spacing;

        // Index of the image selected
        private int selectedIndex;

        private System.Drawing.Rectangle thumbRect;

        // controls text propertie private field
        private System.String thumbControlText;

        private bool resizeRedraw;

        private FlowLayoutPanel pnlFlow;

        private int insertIndex;

        private System.Drawing.Bitmap insertImage;

        // The thumbnail images in the current project
        private List<IndexedImage> images = new List<IndexedImage>();

        // For the the ReadImagesFromProject thread
        //private IAsyncResult async;

        Thread refreshImgsThread;


        #endregion



        #region contructor/destructor


        /// <summary>
        /// Initializes a new instance of the <see cref="ThumbNailViewerExtra" /> class.
        /// </summary>
        /// <param name="FreeProj">The free project reference.</param>
        public ThumbNailViewerExtra(CIPVSFreeProject FreeProj)
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);
            this.UpdateStyles();

            InitializeComponent();

            IPVSFreeProject = FreeProj;
            selectedIndex = -1;

            this.pnlFlow.Click += new EventHandler(pnlFlow_Click);
            this.tmiDeleteImage.Click += new EventHandler(tmiDeleteImage_Click);
        }


        /// <summary>
        /// Finalizes an instance of the <see cref="ThumbNailViewerExtra" /> class.
        /// </summary>
        ~ThumbNailViewerExtra()
        {

            this.pnlFlow.Click -= new EventHandler(pnlFlow_Click);

        }


        #endregion


        #region events


        /// <summary>
        /// Occurs when selected image must be deleted.
        /// </summary>
        public event IPVSSpecials.IPVSEventHandler DeleteImage;


        /// <summary>
        /// Delegate that is needed for various events concerning image selection
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ImageSelectedEventArgs" /> instance containing the event data.</param>
        public delegate void ImageSelectedEventHandler(object sender, ImageSelectedEventArgs e);


        /// <summary>
        /// Occurs when [picture selected].
        /// </summary>
        public event ImageSelectedEventHandler ImageSelected;


        #endregion


        #region properties

        /// <summary>
        /// Gets or sets the index of the image to be inserted in the images list.
        /// </summary>
        /// <value>
        /// The index of the insert.
        /// </value>
        public int InsertIndex
        {
            get
            {
                return this.insertIndex;
            }
            set
            {
                if (this.insertIndex != value)
                {
                    this.insertIndex = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the image to be inserted to the images list.
        /// </summary>
        /// <value>
        /// The insert image.
        /// </value>
        public System.Drawing.Bitmap InsertImage
        {
            get
            {
                return this.insertImage;
            }
            set
            {
                if (value != null)
                {
                    if (this.insertImage != null)
                    {
                        this.insertImage.Dispose();
                        this.insertImage = null;
                    }

                    this.insertImage = value;
                }

            }
        }


        ///// <summary>
        ///// Gets or sets the controls text
        ///// </summary>
        //public override string Text
        //{
        //    get
        //    {
        //        return base.Text;
        //    }
        //    set
        //    {
        //        this.thumbControlText = value;
        //        base.Text = thumbControlText;
        //    }
        //}


        /// <summary>
        /// Gets or sets the size in pixels of the ThumbControl square.
        /// </summary>
        /// <value>
        /// The dimension value.
        /// </value>
        public System.Drawing.Size Dimension
        {
            get
            {
                return dimension;
            }
            set
            {
                dimension = value;
            }
        }


        /// <summary>
        /// Gets or sets the space between the thumbnail controls.
        /// </summary>
        /// <value>
        /// The spacing.
        /// </value>
        public int Spacing
        {
            get
            {
                return spacing;
            }
            set
            {
                spacing = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the control redraws itself when resized.
        /// </summary>
        /// <returns>true if the control redraws itself when resized; otherwise, false.</returns>
        public new bool ResizeRedraw
        {
            get
            {
                return resizeRedraw;
            }
            set
            {
                if (this.resizeRedraw != value)
                {
                    this.resizeRedraw = value;
                    sizeChanged();
                }
            }
        }

        #endregion



        #region methods


        #region public methods


        /// <summary>
        /// Updates the thumbnails.
        /// </summary>
        public void UpdateThumbnails()
        {
            RefreshImages();
        }

        /// <summary>
        /// Starts the loading images.
        /// </summary>
        public void StartLoadingImages()
        {
            RefreshImages();
        }


        /// <summary>
        /// Refreshes the controls thumbnails
        /// </summary>
        public void RefreshImages()
        {
            GetImages();
        }


        /// <summary>
        /// When the flowLayoutPanel is being clicked, all ThumbControls should become unselected
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void pnlFlow_Click(object sender, EventArgs e)
        {
            foreach (ThumbControl tc in this.pnlFlow.Controls)
            {
                tc.Selected = false;
            }
            selectedIndex = -1;
        }


        #endregion


        #region protected methods

        /// <summary>
        /// Occurs when parent control changes it's size and the thumbnailViewer needs to be redrawn
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnSizeChanged(EventArgs e)
        {
            sizeChanged();
            base.OnSizeChanged(e);
        }


        #endregion


        #region private methods


        /// <summary>
        /// Initializes fields
        /// </summary>
        private void initFields()
        {
            insertIndex = -1;
            insertImage = null;
        }


        /// <summary>
        /// Keeps the userControls size on the clientSize of the parent controls
        /// </summary>
        private void sizeChanged()
        {
            this.pnlFlow.SetBounds(this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width, this.ClientRectangle.Height);
            this.Invalidate();
        }


        /// <summary>
        /// Creates a new thread and starts the thumbNailcreation
        /// </summary>
        private void GetImages()
        {

            if (this.IsHandleCreated)
            {

                // number of images changed
                if (this.images.Count != this.IPVSFreeProject.Count)
                {
                    //ThreadPool.QueueUserWorkItem(new WaitCallback(this.ReadImagesFromProject));

                    //if (refreshImgsThread.ThreadState == ThreadState.Running)
                    if (refreshImgsThread != null)
                    {
                        refreshImgsThread.Abort();
                        refreshImgsThread = null;
                    }
                    refreshImgsThread = new Thread(new ThreadStart(this.ReadImagesFromProject));
                    refreshImgsThread.IsBackground = true;
                    refreshImgsThread.Start();

                }
                else if (this.insertIndex >= 0 && (this.images.Count == this.IPVSFreeProject.Count))
                {
                    if (refreshImgsThread != null)
                    {
                        refreshImgsThread.Abort();
                        refreshImgsThread = null;
                    }
                    refreshImgsThread = new Thread(new ThreadStart(this.refreshImageList));
                    refreshImgsThread.IsBackground = true;
                    refreshImgsThread.Start();

                }
                else if (this.insertIndex < 0)
                {
                    //ThreadPool.QueueUserWorkItem(new WaitCallback(this.ReadImagesFromProject));

                    //if (refreshImgsThread != null)
                    //{
                    //    refreshImgsThread.Abort();
                    //    refreshImgsThread = null;
                    //}
                    ////refreshImgsThread = new Thread(new ThreadStart(this.ReadImagesFromProject)); // why??
                    //refreshImgsThread = new Thread(new ThreadStart(this.refreshImageList)); // this should be sufficient !!
                    //refreshImgsThread.IsBackground = true;
                    //refreshImgsThread.Start();
                    this.Invalidate();
                }
            }

            // Create a delegate that points to the method
            // that you want to run asynchronously.
            // The MethodInvoker delegate works because it
            // matches the signature (no parameters or return value).
            //MethodInvoker asyncTask = new MethodInvoker(this.ReadImagesFromProject);

            //// Borrow a thread from the CLR if handle created
            //if (this.IsHandleCreated)
            //    async = asyncTask.BeginInvoke(new AsyncCallback(this.ControlCallback), asyncTask);
        }


        ///// <summary>
        ///// Callback for the ReadImagesFromProject method thread to call EndInvoke
        ///// </summary>
        ///// <param name="async"></param>
        //private void ControlCallback(IAsyncResult async)
        //{

        //    MethodInvoker asyncTask = (MethodInvoker)async.AsyncState;

        //    if (this.IsHandleCreated)
        //        asyncTask.EndInvoke(async);

        //}


        /// <summary>
        /// Clears resources
        /// </summary>
        private void clearResources()
        {
            if (this.insertImage != null)

                this.insertImage.Dispose();
        }




        /// <summary>
        /// Reads images from a project object <paramref name="o"/> and creates thumbnailImages from each image in the project
        /// </summary>
        private void ReadImagesFromProject()
        {

            try
            {

                lock (images)
                {
                    foreach (IndexedImage nImg in images)
                        nImg.clearNamedImage();

                    this.images.Clear();


                    if (IPVSFreeProject != null)
                    {
                        int count = 0;
                        foreach (CIPVSImage img in IPVSFreeProject)
                        {

                            try
                            {
                                using (System.Drawing.Bitmap ImageToThumb = img.getDisplayImage())
                                {

                                    thumbRect = scaleImage(ImageToThumb.Size, new Size(dimension.Width, dimension.Height));
                                    Bitmap thumbnail = new Bitmap(dimension.Width, dimension.Height);


                                    using (System.Drawing.Graphics g = Graphics.FromImage(thumbnail))
                                    {
                                        g.DrawImage(ImageToThumb, thumbRect, new Rectangle(0, 0, ImageToThumb.Width, ImageToThumb.Height), GraphicsUnit.Pixel);
                                        images.Add(new IndexedImage(thumbnail, img.Caption, count, ImageToThumb.Size));
                                    }

                                }

                                count++;
                            }
                            catch
                            {
                                // if there is no file, draw a red Error-Image
                                using (System.Drawing.Image ImageToThumb = new System.Drawing.Bitmap(100, 100))
                                {
                                    using (System.Drawing.Graphics g = Graphics.FromImage(ImageToThumb))
                                    {
                                        g.FillRectangle(Brushes.Red, new Rectangle(new Point(0, 0), ImageToThumb.Size));
                                        thumbRect = scaleImage(ImageToThumb.Size, new Size(dimension.Width, dimension.Height));

                                        Image thumbnail = ImageToThumb.GetThumbnailImage(thumbRect.Width, thumbRect.Height, null, IntPtr.Zero);
                                        images.Add(new IndexedImage(thumbnail, img.Caption, count, ImageToThumb.Size));
                                    }
                                }
                                count++;
                            }
                        }

                        if (!this.IPVSFreeProject.SuppressHasChangedEvent)
                            this.IPVSFreeProject.releaseImageOnAllIPImages();
                    }
                }

                
                if (this.images.Count == this.IPVSFreeProject.Count)
                {

                    if (this.IsHandleCreated && !this.Disposing && !this.IsDisposed)
                        if (this.InvokeRequired)
                            this.Invoke(new MethodInvoker(this.UpdateDisplay));
                        else
                            UpdateDisplay();
                }

            }
            finally
            {
                this.clearResources();
            }
        }



        /// <summary>
        /// Refreshes the image list.
        /// </summary>
        private void refreshImageList()
        {
            try
            {
                lock (images)
                {
                    int index = this.InsertIndex;
                    //System.Drawing.Bitmap image = this.InsertImage;

                    try
                    {
                        using (System.Drawing.Image ImageToThumb = ((IPVS.IPVSBaseClasses.CIPVSImage)this.IPVSFreeProject[index]).getDisplayImage())
                        {
                            thumbRect = scaleImage(ImageToThumb.Size, new Size(dimension.Width, dimension.Height));
                            Bitmap thumbnail = new Bitmap(dimension.Width, dimension.Height);

                            using (System.Drawing.Graphics g = Graphics.FromImage(thumbnail))
                            {
                                g.DrawImage(ImageToThumb, thumbRect, new Rectangle(0, 0, ImageToThumb.Width, ImageToThumb.Height), GraphicsUnit.Pixel);
                                this.images.RemoveAt(index);
                                this.images.Insert(index, new IndexedImage(
                                    thumbnail, 
                                    ((IPVS.IPVSBaseClasses.CIPVSImage)this.IPVSFreeProject[index]).ImageFilePath, 
                                    index,
                                    ImageToThumb.Size));
                            }
                        }
                    }
                    catch
                    {
                        using (System.Drawing.Image ImageToThumb = new System.Drawing.Bitmap(100, 100))
                        {
                            using (System.Drawing.Graphics g = Graphics.FromImage(ImageToThumb))
                            {
                                g.FillRectangle(Brushes.Red, new Rectangle(new Point(0, 0), ImageToThumb.Size));
                                thumbRect = scaleImage(ImageToThumb.Size, new Size(dimension.Width, dimension.Height));

                                Image thumbnail = ImageToThumb.GetThumbnailImage(thumbRect.Width, thumbRect.Height, null, IntPtr.Zero);
                                this.images.Insert(index, new IndexedImage(thumbnail, "Error", index));
                            }
                        }
                    }

                    if (!this.IPVSFreeProject.SuppressHasChangedEvent)
                        this.IPVSFreeProject.releaseImageOnAllIPImages();

                }


                this.InsertIndex = -1;

                if (this.images.Count == this.IPVSFreeProject.Count)
                {

                    if (this.IsHandleCreated)
                        this.Invoke(new MethodInvoker(this.UpdateDisplay));
                }

            }
            //catch (Exception ex)
            //{

            //}
            finally
            {
                this.clearResources();
            }
        }

        
        /// <summary>
        /// Updates the control
        /// </summary>
        private void UpdateDisplay()
        {

            lock (images)
            {

                // Suspend layout to prevent multiple window refreshes.
                pnlFlow.SuspendLayout();

                foreach (ThumbControl tc in this.pnlFlow.Controls)
                {
                    tc.Selected = false;
                }

                // Clear the current display.
                foreach (Control ctrl in pnlFlow.Controls)
                {
                    ctrl.Dispose();
                }
                pnlFlow.Controls.Clear();

                // if image hes been selected before, it stays selected after refresh
                int count = 0;

                // Row and Col will track the current position where pictures are
                // being inserted. They begin at the top-right corner.
                pnlFlow.Padding = new Padding(border);

                // Iterate through the Images collection, and create PictureBox controls.

                foreach (IndexedImage image in images)
                {

                    ThumbControl pic = new ThumbControl(image.ImageIndex, image.Caption, image.ImageSize);

                    // Enable Tooltips on the pictureBoxes
                    ToolTip tt = new ToolTip();
                    tt.SetToolTip(pic, "Image @ index: " + (image.ImageIndex).ToString() + " , Caption: " + image.Caption + " , Size" +
                        "(" + image.ImageSize.Width.ToString() + ", " + image.ImageSize.Height.ToString() + ")");

                    pic.ThumbNailImg = image.Image;
                    pic.ParentPainted = false;
                    pic.Size = new Size(dimension.Width, dimension.Height);
                    pic.Margin = new Padding(spacing);
                    //pic.BorderStyle = BorderStyle.None;
                    pic.BorderStyle = BorderStyle.FixedSingle;
                    pic.BackgroundImageLayout = ImageLayout.Center;

                    pic.Selected = false;

                    // Display the picture.
                    pnlFlow.Controls.Add(pic);

                    if (selectedIndex >= 0)
                    {
                        if (count == selectedIndex)
                            pic.Selected = true;
                    }

                    // Handle the event.
                    //pic.Click += new EventHandler(this.pic_Click);
                    pic.MouseClick += new MouseEventHandler(pic_MouseClick);
                    count++;
                }
                pnlFlow.ResumeLayout();

                sizeChanged();
            }

        }


        /// <summary>
        /// Called when [picture selected].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ImageSelectedEventArgs"/> instance containing the event data.</param>
        private void OnImageSelected(object sender, ImageSelectedEventArgs e)
        {
            if (ImageSelected != null)
            {
                ImageSelected(this, e);
            }
        }


        /// <summary>
        /// Called when [image deleted].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="IPVS.IPVSSpecials.CIPVSGenericEventArgs"/> instance containing the event data.</param>
        private void OnImageDeleted(object sender, IPVS.IPVSSpecials.CIPVSGenericEventArgs e)
        {
            if (DeleteImage != null)
            {
                DeleteImage(this, e);
            }
        }


        /// <summary>
        /// Method that returns a rectangle that fits best into a viewport
        /// </summary>
        /// <param name="ImageSize">The size of a given image</param>
        /// <param name="DisplaySize">The size of the viewport</param>
        /// <returns></returns>
        private System.Drawing.Rectangle scaleImage(System.Drawing.Size ImageSize, System.Drawing.Size DisplaySize)
        {
            System.Drawing.Rectangle TempRect = new Rectangle();

            if ((((float)ImageSize.Width) / ((float)ImageSize.Height)) >= (((float)DisplaySize.Width) / ((float)DisplaySize.Height)))
            {

                float tempFactor = ((float)DisplaySize.Width / (float)ImageSize.Width);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.Y = (int)Math.Round((((float)(DisplaySize.Height) / (2.0f)) - ((float)(TempRect.Height) / (2.0f))));

            }
            //if ((ImageSize.Width / ImageSize.Height) < (DisplaySize.Width / DisplaySize.Height))
            else
            {

                float tempFactor = ((float)DisplaySize.Height / (float)ImageSize.Height);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.X = (int)Math.Round(((DisplaySize.Width) / (2.0f)) - ((float)(TempRect.Width) / (2.0f)));

            }

            return TempRect;

        }

        #endregion


        #region event handler


        /// <summary>
        /// Handles the MouseClick event of the pic control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void pic_MouseClick(object sender, MouseEventArgs e)
        {

            // Clear the border style from the last selected thumbcontrol.
            if (thumbControlSelected != null)
            {
                //thumbControlSelected.BorderStyle = BorderStyle.None;
                thumbControlSelected.Selected = false;
            }

            // Get the new selection.
            thumbControlSelected = (ThumbControl)sender;

            thumbControlSelected.Selected = true;
            thumbControlSelected.BorderStyle = BorderStyle.FixedSingle;

            this.selectedIndex = thumbControlSelected.ImageInformation.Index;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                //this.cmsThumbControl.
                Point newPos = thumbControlSelected.PointToScreen(e.Location);
                //Point newPos = this.PointToScreen(e.Location);
                this.cmsThumbControl.Show(newPos);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                // Fire the selection event.
                ImageSelectedEventArgs args = new
                    ImageSelectedEventArgs(thumbControlSelected.ImageInformation.Caption, this.selectedIndex);
                OnImageSelected(this, args);
            }
        }

        /// <summary>
        /// Handles the Click event of the tmiDeleteImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tmiDeleteImage_Click(object sender, EventArgs e)
        {
            this.OnImageDeleted(this, new IPVS.IPVSSpecials.CIPVSGenericEventArgs(this.selectedIndex));
        }

        #endregion

        #endregion
    }

}
