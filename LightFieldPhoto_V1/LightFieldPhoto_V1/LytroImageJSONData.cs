﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightFieldPhoto_V1
{

    public class LytroImageJSONData
    {
        private string representation;  // "representation" : "jpeg",
        private int width;  // "width" : 1080,
        private int height; // "height" : 1080,
        private double lambda; //	"lambda" : -12.537451744079589844,
        private string imageRef; //	"imageRef" : "sha1-bddffa504ceaf4eb368481a9f07ae76891a3f647"

        public string Representation
        {
            get
            {
                return this.representation;
            }
            set
            {
                this.representation = value;
            }
        }
        public int Width
        {
            get
            {
                return this.width;
            }
            set
            {
                this.width = value;
            }
        }
        public int Height
        {
            get
            {
                return this.height;
            }
            set
            {
                this.height = value;
            }
        }
        public double Lambda
        {
            get
            {
                return this.lambda;
            }
            set
            {
                this.lambda = value;
            }
        }
        public string ImageRef
        {
            get
            {
                return this.imageRef;
            }
            set
            {
                this.imageRef = value;
            }
        }
    }
}
