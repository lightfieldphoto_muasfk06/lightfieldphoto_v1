﻿namespace LightFieldPhoto_V1
{
    partial class LytroPackageViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (fileDialog != null)
                fileDialog.Dispose();
            if (Package != null)
                Package = null;

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LytroPackageViewerForm));
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.fileStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.filePathStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractJSONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.previousImageStackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextImageStackToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbOpenPackage = new System.Windows.Forms.ToolStripButton();
            this.tsbExtractImages = new System.Windows.Forms.ToolStripButton();
            this.tsbReadJSON = new System.Windows.Forms.ToolStripButton();
            this.tsbSplitPackage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPreviousImage = new System.Windows.Forms.ToolStripButton();
            this.tsbNextImage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.stcImageBoxTabControlPackage = new System.Windows.Forms.SplitContainer();
            this.tbcImageBoxThumNails = new System.Windows.Forms.TabControl();
            this.tbpImageBox = new System.Windows.Forms.TabPage();
            this.imageBox = new IPVS.IPVSImageBox.ImageBox();
            this.tbpStackPreview = new System.Windows.Forms.TabPage();
            this.richTextBoxTest = new System.Windows.Forms.RichTextBox();
            this.tbpJSONFileViewer = new System.Windows.Forms.TabPage();
            this.FCTextBoxJSON = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabControlProperties = new System.Windows.Forms.TabControl();
            this.tbpJSONFile = new System.Windows.Forms.TabPage();
            this.propertyGridJSON = new System.Windows.Forms.PropertyGrid();
            this.tbpPackageProperties = new System.Windows.Forms.TabPage();
            this.propertyGridPackage = new System.Windows.Forms.PropertyGrid();
            this.tbpImageBoxProperties = new System.Windows.Forms.TabPage();
            this.propertyGridImageBox = new System.Windows.Forms.PropertyGrid();
            this.statusBar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stcImageBoxTabControlPackage)).BeginInit();
            this.stcImageBoxTabControlPackage.Panel1.SuspendLayout();
            this.stcImageBoxTabControlPackage.Panel2.SuspendLayout();
            this.stcImageBoxTabControlPackage.SuspendLayout();
            this.tbcImageBoxThumNails.SuspendLayout();
            this.tbpImageBox.SuspendLayout();
            this.tbpStackPreview.SuspendLayout();
            this.tbpJSONFileViewer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FCTextBoxJSON)).BeginInit();
            this.tabControlProperties.SuspendLayout();
            this.tbpJSONFile.SuspendLayout();
            this.tbpPackageProperties.SuspendLayout();
            this.tbpImageBoxProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileStatusLabel,
            this.filePathStatusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 633);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1070, 22);
            this.statusBar.TabIndex = 4;
            // 
            // fileStatusLabel
            // 
            this.fileStatusLabel.Name = "fileStatusLabel";
            this.fileStatusLabel.Size = new System.Drawing.Size(28, 17);
            this.fileStatusLabel.Text = "File:";
            // 
            // filePathStatusLabel
            // 
            this.filePathStatusLabel.Name = "filePathStatusLabel";
            this.filePathStatusLabel.Size = new System.Drawing.Size(1027, 17);
            this.filePathStatusLabel.Spring = true;
            this.filePathStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.packageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1070, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openFileToolStripMenuItem.Text = "&Open Lytro Package";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.closeAllToolStripMenuItem.Text = "&Close All";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.closeAllToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            // 
            // packageToolStripMenuItem
            // 
            this.packageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extractImagesToolStripMenuItem,
            this.extractJSONToolStripMenuItem,
            this.toolStripMenuItem2,
            this.previousImageStackToolStripMenuItem,
            this.nextImageStackToolStripMenuItem1,
            this.toolStripMenuItem3});
            this.packageToolStripMenuItem.Name = "packageToolStripMenuItem";
            this.packageToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.packageToolStripMenuItem.Text = "&Package";
            // 
            // extractImagesToolStripMenuItem
            // 
            this.extractImagesToolStripMenuItem.Name = "extractImagesToolStripMenuItem";
            this.extractImagesToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.extractImagesToolStripMenuItem.Text = "E&xtract Images";
            this.extractImagesToolStripMenuItem.Click += new System.EventHandler(this.extractImagesToolStripMenuItem_Click);
            // 
            // extractJSONToolStripMenuItem
            // 
            this.extractJSONToolStripMenuItem.Name = "extractJSONToolStripMenuItem";
            this.extractJSONToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.extractJSONToolStripMenuItem.Text = "Extract &JSON";
            this.extractJSONToolStripMenuItem.Click += new System.EventHandler(this.extractJSONToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(191, 6);
            // 
            // previousImageStackToolStripMenuItem
            // 
            this.previousImageStackToolStripMenuItem.Name = "previousImageStackToolStripMenuItem";
            this.previousImageStackToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.previousImageStackToolStripMenuItem.Text = "&Previous Image (Stack)";
            this.previousImageStackToolStripMenuItem.Click += new System.EventHandler(this.previousImageStackToolStripMenuItem_Click);
            // 
            // nextImageStackToolStripMenuItem1
            // 
            this.nextImageStackToolStripMenuItem1.Name = "nextImageStackToolStripMenuItem1";
            this.nextImageStackToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.nextImageStackToolStripMenuItem1.Text = "&Next Image (Stack)";
            this.nextImageStackToolStripMenuItem1.Click += new System.EventHandler(this.nextImageStackToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(191, 6);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbOpenPackage,
            this.tsbExtractImages,
            this.tsbReadJSON,
            this.tsbSplitPackage,
            this.toolStripSeparator1,
            this.tsbPreviousImage,
            this.tsbNextImage,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1070, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbOpenPackage
            // 
            this.tsbOpenPackage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpenPackage.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpenPackage.Image")));
            this.tsbOpenPackage.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbOpenPackage.Name = "tsbOpenPackage";
            this.tsbOpenPackage.Size = new System.Drawing.Size(23, 22);
            this.tsbOpenPackage.Text = "Open Package";
            this.tsbOpenPackage.Click += new System.EventHandler(this.tsbOpenPackage_Click);
            // 
            // tsbExtractImages
            // 
            this.tsbExtractImages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbExtractImages.Image = ((System.Drawing.Image)(resources.GetObject("tsbExtractImages.Image")));
            this.tsbExtractImages.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbExtractImages.Name = "tsbExtractImages";
            this.tsbExtractImages.Size = new System.Drawing.Size(23, 22);
            this.tsbExtractImages.Text = "Extract Images";
            this.tsbExtractImages.ToolTipText = "Extract Images";
            this.tsbExtractImages.Click += new System.EventHandler(this.tsbExtractImages_Click);
            // 
            // tsbReadJSON
            // 
            this.tsbReadJSON.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReadJSON.Image = ((System.Drawing.Image)(resources.GetObject("tsbReadJSON.Image")));
            this.tsbReadJSON.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbReadJSON.Name = "tsbReadJSON";
            this.tsbReadJSON.Size = new System.Drawing.Size(23, 22);
            this.tsbReadJSON.Text = "Extract JSON";
            this.tsbReadJSON.Click += new System.EventHandler(this.tsbReadJSON_Click);
            // 
            // tsbSplitPackage
            // 
            this.tsbSplitPackage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSplitPackage.Image = ((System.Drawing.Image)(resources.GetObject("tsbSplitPackage.Image")));
            this.tsbSplitPackage.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbSplitPackage.Name = "tsbSplitPackage";
            this.tsbSplitPackage.Size = new System.Drawing.Size(23, 22);
            this.tsbSplitPackage.Text = "Split Package";
            this.tsbSplitPackage.Click += new System.EventHandler(this.tsbSplitPackage_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbPreviousImage
            // 
            this.tsbPreviousImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPreviousImage.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreviousImage.Image")));
            this.tsbPreviousImage.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbPreviousImage.Name = "tsbPreviousImage";
            this.tsbPreviousImage.Size = new System.Drawing.Size(23, 22);
            this.tsbPreviousImage.Text = "Previous Image";
            this.tsbPreviousImage.Click += new System.EventHandler(this.tsbPreviousImage_Click);
            // 
            // tsbNextImage
            // 
            this.tsbNextImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNextImage.Image = ((System.Drawing.Image)(resources.GetObject("tsbNextImage.Image")));
            this.tsbNextImage.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tsbNextImage.Name = "tsbNextImage";
            this.tsbNextImage.Size = new System.Drawing.Size(23, 22);
            this.tsbNextImage.Text = "Next Image";
            this.tsbNextImage.Click += new System.EventHandler(this.tsbNextImage_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // stcImageBoxTabControlPackage
            // 
            this.stcImageBoxTabControlPackage.BackColor = System.Drawing.SystemColors.Control;
            this.stcImageBoxTabControlPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stcImageBoxTabControlPackage.Location = new System.Drawing.Point(0, 49);
            this.stcImageBoxTabControlPackage.Name = "stcImageBoxTabControlPackage";
            // 
            // stcImageBoxTabControlPackage.Panel1
            // 
            this.stcImageBoxTabControlPackage.Panel1.Controls.Add(this.tbcImageBoxThumNails);
            // 
            // stcImageBoxTabControlPackage.Panel2
            // 
            this.stcImageBoxTabControlPackage.Panel2.Controls.Add(this.tabControlProperties);
            this.stcImageBoxTabControlPackage.Size = new System.Drawing.Size(1070, 584);
            this.stcImageBoxTabControlPackage.SplitterDistance = 731;
            this.stcImageBoxTabControlPackage.TabIndex = 7;
            this.stcImageBoxTabControlPackage.MouseEnter += new System.EventHandler(this.stcImageBoxTabControlPackage_MouseEnter);
            this.stcImageBoxTabControlPackage.MouseLeave += new System.EventHandler(this.stcImageBoxTabControlPackage_MouseLeave);
            // 
            // tbcImageBoxThumNails
            // 
            this.tbcImageBoxThumNails.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tbcImageBoxThumNails.Controls.Add(this.tbpImageBox);
            this.tbcImageBoxThumNails.Controls.Add(this.tbpStackPreview);
            this.tbcImageBoxThumNails.Controls.Add(this.tbpJSONFileViewer);
            this.tbcImageBoxThumNails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcImageBoxThumNails.Location = new System.Drawing.Point(0, 0);
            this.tbcImageBoxThumNails.Name = "tbcImageBoxThumNails";
            this.tbcImageBoxThumNails.SelectedIndex = 0;
            this.tbcImageBoxThumNails.Size = new System.Drawing.Size(731, 584);
            this.tbcImageBoxThumNails.TabIndex = 1;
            // 
            // tbpImageBox
            // 
            this.tbpImageBox.Controls.Add(this.imageBox);
            this.tbpImageBox.Location = new System.Drawing.Point(4, 25);
            this.tbpImageBox.Name = "tbpImageBox";
            this.tbpImageBox.Padding = new System.Windows.Forms.Padding(3);
            this.tbpImageBox.Size = new System.Drawing.Size(723, 555);
            this.tbpImageBox.TabIndex = 0;
            this.tbpImageBox.Text = "Recent Image";
            this.tbpImageBox.UseVisualStyleBackColor = true;
            // 
            // imageBox
            // 
            this.imageBox.AutoPan = false;
            this.imageBox.AutoSize = false;
            this.imageBox.CenterImage = false;
            this.imageBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageBox.ImageViewPort = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.imageBox.Location = new System.Drawing.Point(3, 3);
            this.imageBox.Name = "imageBox";
            this.imageBox.Size = new System.Drawing.Size(717, 549);
            this.imageBox.SizeToFit = true;
            this.imageBox.TabIndex = 0;
            this.imageBox.Zoom = 100F;
            // 
            // tbpStackPreview
            // 
            this.tbpStackPreview.Controls.Add(this.richTextBoxTest);
            this.tbpStackPreview.Location = new System.Drawing.Point(4, 25);
            this.tbpStackPreview.Name = "tbpStackPreview";
            this.tbpStackPreview.Padding = new System.Windows.Forms.Padding(3);
            this.tbpStackPreview.Size = new System.Drawing.Size(723, 555);
            this.tbpStackPreview.TabIndex = 1;
            this.tbpStackPreview.Text = "Stack Preview";
            this.tbpStackPreview.UseVisualStyleBackColor = true;
            // 
            // richTextBoxTest
            // 
            this.richTextBoxTest.Location = new System.Drawing.Point(8, 6);
            this.richTextBoxTest.Name = "richTextBoxTest";
            this.richTextBoxTest.Size = new System.Drawing.Size(546, 291);
            this.richTextBoxTest.TabIndex = 0;
            this.richTextBoxTest.Text = "";
            // 
            // tbpJSONFileViewer
            // 
            this.tbpJSONFileViewer.Controls.Add(this.FCTextBoxJSON);
            this.tbpJSONFileViewer.Location = new System.Drawing.Point(4, 25);
            this.tbpJSONFileViewer.Name = "tbpJSONFileViewer";
            this.tbpJSONFileViewer.Padding = new System.Windows.Forms.Padding(3);
            this.tbpJSONFileViewer.Size = new System.Drawing.Size(723, 555);
            this.tbpJSONFileViewer.TabIndex = 2;
            this.tbpJSONFileViewer.Text = "JSON File";
            this.tbpJSONFileViewer.UseVisualStyleBackColor = true;
            // 
            // FCTextBoxJSON
            // 
            this.FCTextBoxJSON.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.FCTextBoxJSON.AutoIndentCharsPatterns = "\r\n^\\s*[\\w\\.]+(\\s\\w+)?\\s*(?<range>=)\\s*(?<range>[^;]+);\r\n";
            this.FCTextBoxJSON.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.FCTextBoxJSON.BackBrush = null;
            this.FCTextBoxJSON.BracketsHighlightStrategy = FastColoredTextBoxNS.BracketsHighlightStrategy.Strategy2;
            this.FCTextBoxJSON.CharHeight = 14;
            this.FCTextBoxJSON.CharWidth = 8;
            this.FCTextBoxJSON.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.FCTextBoxJSON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.FCTextBoxJSON.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FCTextBoxJSON.IsReplaceMode = false;
            this.FCTextBoxJSON.Language = FastColoredTextBoxNS.Language.JS;
            this.FCTextBoxJSON.LeftBracket = '(';
            this.FCTextBoxJSON.LeftBracket2 = '{';
            this.FCTextBoxJSON.Location = new System.Drawing.Point(3, 3);
            this.FCTextBoxJSON.Name = "FCTextBoxJSON";
            this.FCTextBoxJSON.Paddings = new System.Windows.Forms.Padding(0);
            this.FCTextBoxJSON.ReadOnly = true;
            this.FCTextBoxJSON.RightBracket = ')';
            this.FCTextBoxJSON.RightBracket2 = '}';
            this.FCTextBoxJSON.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.FCTextBoxJSON.Size = new System.Drawing.Size(717, 549);
            this.FCTextBoxJSON.TabIndex = 0;
            this.FCTextBoxJSON.Zoom = 100;
            // 
            // tabControlProperties
            // 
            this.tabControlProperties.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlProperties.Controls.Add(this.tbpJSONFile);
            this.tabControlProperties.Controls.Add(this.tbpPackageProperties);
            this.tabControlProperties.Controls.Add(this.tbpImageBoxProperties);
            this.tabControlProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlProperties.Location = new System.Drawing.Point(0, 0);
            this.tabControlProperties.Name = "tabControlProperties";
            this.tabControlProperties.SelectedIndex = 0;
            this.tabControlProperties.Size = new System.Drawing.Size(335, 584);
            this.tabControlProperties.TabIndex = 0;
            // 
            // tbpJSONFile
            // 
            this.tbpJSONFile.Controls.Add(this.propertyGridJSON);
            this.tbpJSONFile.Location = new System.Drawing.Point(4, 25);
            this.tbpJSONFile.Name = "tbpJSONFile";
            this.tbpJSONFile.Padding = new System.Windows.Forms.Padding(3);
            this.tbpJSONFile.Size = new System.Drawing.Size(327, 555);
            this.tbpJSONFile.TabIndex = 2;
            this.tbpJSONFile.Text = "JSON Image";
            this.tbpJSONFile.UseVisualStyleBackColor = true;
            // 
            // propertyGridJSON
            // 
            this.propertyGridJSON.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridJSON.Location = new System.Drawing.Point(3, 3);
            this.propertyGridJSON.Name = "propertyGridJSON";
            this.propertyGridJSON.Size = new System.Drawing.Size(321, 549);
            this.propertyGridJSON.TabIndex = 0;
            // 
            // tbpPackageProperties
            // 
            this.tbpPackageProperties.Controls.Add(this.propertyGridPackage);
            this.tbpPackageProperties.Location = new System.Drawing.Point(4, 25);
            this.tbpPackageProperties.Name = "tbpPackageProperties";
            this.tbpPackageProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPackageProperties.Size = new System.Drawing.Size(327, 555);
            this.tbpPackageProperties.TabIndex = 0;
            this.tbpPackageProperties.Text = "Package Properties";
            this.tbpPackageProperties.UseVisualStyleBackColor = true;
            // 
            // propertyGridPackage
            // 
            this.propertyGridPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridPackage.Location = new System.Drawing.Point(3, 3);
            this.propertyGridPackage.Name = "propertyGridPackage";
            this.propertyGridPackage.Size = new System.Drawing.Size(321, 549);
            this.propertyGridPackage.TabIndex = 0;
            // 
            // tbpImageBoxProperties
            // 
            this.tbpImageBoxProperties.Controls.Add(this.propertyGridImageBox);
            this.tbpImageBoxProperties.Location = new System.Drawing.Point(4, 25);
            this.tbpImageBoxProperties.Name = "tbpImageBoxProperties";
            this.tbpImageBoxProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tbpImageBoxProperties.Size = new System.Drawing.Size(327, 555);
            this.tbpImageBoxProperties.TabIndex = 1;
            this.tbpImageBoxProperties.Text = "ImageBox Settings";
            this.tbpImageBoxProperties.UseVisualStyleBackColor = true;
            // 
            // propertyGridImageBox
            // 
            this.propertyGridImageBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridImageBox.Location = new System.Drawing.Point(3, 3);
            this.propertyGridImageBox.Name = "propertyGridImageBox";
            this.propertyGridImageBox.SelectedObject = this.imageBox;
            this.propertyGridImageBox.Size = new System.Drawing.Size(321, 549);
            this.propertyGridImageBox.TabIndex = 1;
            // 
            // LytroPackageViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 655);
            this.Controls.Add(this.stcImageBoxTabControlPackage);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LytroPackageViewerForm";
            this.Text = "Lytro Package Images Viewer";
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.stcImageBoxTabControlPackage.Panel1.ResumeLayout(false);
            this.stcImageBoxTabControlPackage.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stcImageBoxTabControlPackage)).EndInit();
            this.stcImageBoxTabControlPackage.ResumeLayout(false);
            this.tbcImageBoxThumNails.ResumeLayout(false);
            this.tbpImageBox.ResumeLayout(false);
            this.tbpStackPreview.ResumeLayout(false);
            this.tbpJSONFileViewer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FCTextBoxJSON)).EndInit();
            this.tabControlProperties.ResumeLayout(false);
            this.tbpJSONFile.ResumeLayout(false);
            this.tbpPackageProperties.ResumeLayout(false);
            this.tbpImageBoxProperties.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FastColoredTextBoxNS.FastColoredTextBox FCTextBoxJSON;
        private IPVS.IPVSImageBox.ImageBox imageBox;
        private System.Windows.Forms.PropertyGrid propertyGridImageBox;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.SplitContainer stcImageBoxTabControlPackage;
        private System.Windows.Forms.ToolStripStatusLabel filePathStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel fileStatusLabel;
        private System.Windows.Forms.TabControl tabControlProperties;
        private System.Windows.Forms.TabPage tbpPackageProperties;
        private System.Windows.Forms.TabPage tbpImageBoxProperties;
        private System.Windows.Forms.PropertyGrid propertyGridPackage;
        private System.Windows.Forms.ToolStripButton tsbOpenPackage;
        private System.Windows.Forms.ToolStripButton tsbSplitPackage;
        private System.Windows.Forms.ToolStripButton tsbExtractImages;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbReadJSON;
        private System.Windows.Forms.ToolStripButton tsbPreviousImage;
        private System.Windows.Forms.ToolStripButton tsbNextImage;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabPage tbpJSONFile;
        private System.Windows.Forms.TabControl tbcImageBoxThumNails;
        private System.Windows.Forms.TabPage tbpImageBox;
        private System.Windows.Forms.TabPage tbpStackPreview;
        private System.Windows.Forms.ToolStripMenuItem packageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extractImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extractJSONToolStripMenuItem;
        private System.Windows.Forms.TabPage tbpJSONFileViewer;
        private System.Windows.Forms.PropertyGrid propertyGridJSON;
        private System.Windows.Forms.RichTextBox richTextBoxTest;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem previousImageStackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextImageStackToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
    }
}

