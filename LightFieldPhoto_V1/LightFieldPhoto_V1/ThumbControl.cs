﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IPVS.IPVSControls
{


    /// <summary>
    /// ThumbControl that displays a thumbnail image and contains image data
    /// </summary>
    public partial class ThumbControl : Control
    {

        #region public fields

        /// <summary>
        /// Information of the image connected with this control
        /// </summary>
        public ImageInfo ImageInformation;

        #endregion


        #region private fields

        private Color backgroundColor;
        private Image thumbNailImg;
        private bool selected;
        private bool parentPainted;
        private Rectangle parentThumbNRectangle;
        private BorderStyle borderStyle;

        #endregion


        #region constructor


        /// <summary>
        /// Initializes a new instance of the <see cref="ThumbControl" /> class.
        /// </summary>
        /// <param name="imageIndex">Index of the image.</param>
        /// <param name="imageCaption">The image caption.</param>
        /// <param name="imageSize">Size of the image.</param>
        public ThumbControl(int imageIndex, string imageCaption, Size imageSize)
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);
            this.UpdateStyles();

            this.BackgroundImageLayout = ImageLayout.Center;

            InitializeComponent();

            this.backgroundColor = Color.FromKnownColor(KnownColor.Control);
            this.parentPainted = false;

            ImageInformation = new ImageInfo(imageIndex, imageCaption, imageSize);
        }

        #endregion


        #region events

        //event IPVSSpecials.IPVSEventHandler DeleteImageEvent;

        #endregion


        #region properties


        #region overriden properties

        /// <summary>
        /// Gets or sets the background color for the control.
        /// </summary>
        /// <returns>A <see cref="T:System.Drawing.Color" /> that represents the background color of the control. The default is the value of the <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property.</returns>
        ///   <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ///   </PermissionSet>
        [DefaultValue(typeof(Color), "Control")]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }

        /// <summary>
        /// Gets or sets the background image displayed in the control.
        /// </summary>
        /// <returns>An <see cref="T:System.Drawing.Image" /> that represents the image to display in the background of the control.</returns>
        ///   <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ///   </PermissionSet>
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Image BackgroundImage
        {
            get { return base.BackgroundImage; }
            set { base.BackgroundImage = value; }
        }

        /// <summary>
        /// Gets or sets the background image layout as defined in the <see cref="T:System.Windows.Forms.ImageLayout" /> enumeration.
        /// </summary>
        /// <returns>One of the values of <see cref="T:System.Windows.Forms.ImageLayout" /> (<see cref="F:System.Windows.Forms.ImageLayout.Center" /> , <see cref="F:System.Windows.Forms.ImageLayout.None" />, <see cref="F:System.Windows.Forms.ImageLayout.Stretch" />, <see cref="F:System.Windows.Forms.ImageLayout.Tile" />, or <see cref="F:System.Windows.Forms.ImageLayout.Zoom" />). <see cref="F:System.Windows.Forms.ImageLayout.Tile" /> is the default value.</returns>
        ///   <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ///   </PermissionSet>
        [Browsable(true), DefaultValue(ImageLayout.Center)]
        public override ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }

        #endregion


        #region public properties


        /// <summary>
        /// Gets or sets the controls thumbnail image.
        /// </summary>
        public Image ThumbNailImg
        {
            get
            {
                return this.thumbNailImg;
            }
            set
            {
                if (value != null)
                {

                    if (this.thumbNailImg != null)
                    {
                        this.thumbNailImg.Dispose();
                        this.thumbNailImg = null;
                    }

                    this.thumbNailImg = new Bitmap(value);

                    // set BackgroundImage !!
                    if (this.BackgroundImage != null)
                    {
                        this.BackgroundImage.Dispose();
                        this.BackgroundImage = null;
                    }

                    this.BackgroundImage = this.thumbNailImg;

                    this.Refresh();
                }

            }
        }



        /// <summary>
        /// Gets or sets the border style.
        /// </summary>
        /// <value>
        /// The border style.
        /// </value>
        public BorderStyle BorderStyle
        {

            get { return this.borderStyle; }
            set
            {
                if (this.borderStyle != value)
                {
                    borderStyle = value;
                    this.Refresh();
                }
            }
        }


        /// <summary>
        /// Gets or sets whether the control is selected or not.
        /// </summary>
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (this.selected != value)
                {
                    this.selected = value;
                    this.Invalidate();
                    this.Update();
                }
            }
        }


        /// <summary>
        /// Gets or sets whether the controls the tumbnails on the controls clientrectangle 
        /// gets painted by custom code or the paintBackground method
        /// </summary>
        public bool ParentPainted
        {
            get
            {
                return parentPainted;
            }
            set
            {
                if (this.parentPainted != value)
                {
                    this.parentPainted = value;
                    this.Invalidate();
                    this.Update();
                }
            }
        }

        /// <summary>
        /// Gets or sets a parent given rectangle for positioning the thumbnail image.
        /// </summary>
        public Rectangle ParentThumbNRectangle
        {
            get
            {
                this.parentThumbNRectangle =
                    scaleSizeToViewPort(this.ThumbNailImg.Size, this.ClientSize);
                return parentThumbNRectangle;
            }
            set
            {
                if ((this.parentPainted) && (this.parentThumbNRectangle != value))
                {
                    parentThumbNRectangle = value;
                    this.Refresh();
                }
            }
        }


       

        #endregion



        #endregion


        #region protected methods

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.DrawRectangle(Pens.Black, new Rectangle(0,0, this.ClientRectangle.Width - 1, this.ClientRectangle.Height - 1));

            // BackGround 
            if (parentPainted)
            {
                using (Brush backBrush = new SolidBrush(this.backgroundColor))
                {
                    g.FillRectangle(backBrush, this.ClientRectangle);
                    g.DrawImageUnscaled(thumbNailImg, this.ParentThumbNRectangle);
                }
            }

            if ((this.thumbNailImg != null) && (this.Selected))
            {
                using (Brush transparentBrush = new SolidBrush(Color.FromArgb(140, 0xc0, 0xc0, 0xc0)))
                {
                    g.FillRectangle(transparentBrush, this.ClientRectangle);
                }

                if (this.BorderStyle == BorderStyle.FixedSingle)
                {
                    using (Pen borderPen = new Pen(Color.BlueViolet, 4))
                    {
                        g.DrawRectangle(borderPen, this.ClientRectangle);
                    }
                }
            }

            base.OnPaint(e);
        }


        /// <summary>
        /// Raises the <see cref="E:PaintBackground" /> event.
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs" /> instance containing the event data.</param>
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (!parentPainted)
            {
                if (this.BackgroundImage != null)
                    base.OnPaintBackground(e);
            }
        }


        /// <summary>
        /// Method that returns a rectangle that fits best into a viewport and represents a given image that must be scaled
        /// </summary>
        /// <param name="ImageSize">Size of the image.</param>
        /// <param name="DisplaySize">The display size.</param>
        /// <returns>The rectangle to the viewport.</returns>
        private static System.Drawing.Rectangle scaleSizeToViewPort(System.Drawing.Size ImageSize, System.Drawing.Size DisplaySize)
        {
            System.Drawing.Rectangle TempRect = new Rectangle();

            if ((((float)ImageSize.Width) / ((float)ImageSize.Height)) >= (((float)DisplaySize.Width) / ((float)DisplaySize.Height)))
            {
                float tempFactor = ((float)DisplaySize.Width / (float)ImageSize.Width);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.Y = (int)Math.Round((((float)(DisplaySize.Height) / (2.0f)) - ((float)(TempRect.Height) / (2.0f))));

            }
            else
            {
                float tempFactor = ((float)DisplaySize.Height / (float)ImageSize.Height);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.X = (int)Math.Round(((DisplaySize.Width) / (2.0f)) - ((float)(TempRect.Width) / (2.0f)));

            }
            return TempRect;
        }




        #endregion


        ///// <summary>
        ///// Handles the MouseClick event of the ThumbControl control.
        ///// </summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        //private void ThumbControl_MouseClick(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == System.Windows.Forms.MouseButtons.Right)
        //    {
        //        //this.cmsThumbControl.
        //        Point newPos = this.PointToScreen(e.Location);
        //        this.cmsThumbControl.Show(newPos);
        //    }
        //}



    }



    /// <summary>
    /// Contains Information about the thumbnails original image
    /// </summary>
    public class ImageInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageInfo"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="size">The size.</param>
        public ImageInfo(int index, string caption, Size size)
        {
            Index = index;
            Caption = caption;
            Size = size;
        }

        /// <summary>
        /// The index
        /// </summary>
        public int Index;
        /// <summary>
        /// The caption
        /// </summary>
        public System.String Caption;
        /// <summary>
        /// The size
        /// </summary>
        public Size Size;
    }

}
