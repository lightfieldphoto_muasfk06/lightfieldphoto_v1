﻿////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;


namespace IPVS.IPVSControls
{
    /// <summary>
    /// Eventargument class for the <see cref="ThumbNailViewerExtra"/> control
    /// </summary>
    public class ImageSelectedEventArgs : EventArgs
    {

        private string caption;
        /// <summary>
        /// Gets or sets the image caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }

        private int index;

        /// <summary>
        /// Index of the selected image.
        /// </summary>
        /// <value>
        /// The index of the image.
        /// </value>
        public int ImageIndex
        {
            get { return index; }
            set { this.index = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSelectedEventArgs" /> class.
        /// </summary>
        /// <param name="index">The index.</param>
        public ImageSelectedEventArgs(int index)
        {
            ImageIndex = index;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSelectedEventArgs" /> class.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="index">The index.</param>
        public ImageSelectedEventArgs(String caption, int index)
        {
            ImageIndex = index;
            this.caption = caption;
        }

    }

}