﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace IPVS.IPVSControls
{

    /// <summary>
    /// Class containing data for the <see cref="ThumbNailViewerExtra"/> control.
    /// </summary>
    public class IndexedImage
    {
        private Image image;
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        public Image Image
        {
            get 
            { 
                return image; 
            }
            set 
            {
                if (this.image != null)
                {
                    this.image.Dispose();
                    this.image = null;
                }
                image = value; 
            }
        }

        private int index;
        /// <summary>
        /// Gets or sets the index of the image.
        /// </summary>
        /// <value>
        /// The index of the image.
        /// </value>
        public int ImageIndex
        {
            get { return index; }
            set { this.index = value; }
        }

        private string caption;
        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }

        private Size size;
        /// <summary>
        /// Gets or sets the size of the image.
        /// </summary>
        /// <value>
        /// The size of the image.
        /// </value>
        public Size ImageSize
        {
            get { return this.size; }
            set { size = value; }
        }

        /// <summary>
        /// Clears the named image.
        /// </summary>
        public void clearNamedImage()
        {
            if (this.image != null)
                image.Dispose();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexedImage" /> class.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="index">The index.</param>
        public IndexedImage(Image image, string fileName, int index)
        {
            this.ImageIndex = index;
            this.Image = image;
            this.Caption = fileName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexedImage" /> class.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="index">The index.</param>
        /// <param name="imgSize">Size of the img.</param>
        public IndexedImage(Image image, string fileName, int index, Size imgSize)
        {
            this.ImageIndex = index;
            this.Image = image;
            this.Caption = fileName;
            this.ImageSize = imgSize;
        }

    }
}
