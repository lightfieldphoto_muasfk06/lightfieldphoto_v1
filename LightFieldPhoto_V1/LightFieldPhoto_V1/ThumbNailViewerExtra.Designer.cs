﻿namespace IPVS.IPVSControls
{
    partial class ThumbNailViewerExtra
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.tmiDeleteImage = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsThumbControl = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsThumbControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFlow
            // 
            this.pnlFlow.AutoScroll = true;
            this.pnlFlow.AutoSize = true;
            this.pnlFlow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlFlow.BackColor = System.Drawing.Color.Silver;
            this.pnlFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFlow.Location = new System.Drawing.Point(0, 0);
            this.pnlFlow.Name = "pnlFlow";
            this.pnlFlow.Size = new System.Drawing.Size(300, 300);
            this.pnlFlow.TabIndex = 0;
            // 
            // tmiDeleteImage
            // 
            this.tmiDeleteImage.Name = "tmiDeleteImage";
            this.tmiDeleteImage.Size = new System.Drawing.Size(183, 22);
            this.tmiDeleteImage.Text = "Delete Project Image";
            // 
            // cmsThumbControl
            // 
            this.cmsThumbControl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmiDeleteImage});
            this.cmsThumbControl.Name = "cmsThumbControl";
            this.cmsThumbControl.Size = new System.Drawing.Size(184, 48);
            // 
            // ThumbNailViewerExtra
            // 
            this.AutoSize = true;
            this.Controls.Add(this.pnlFlow);
            this.Name = "ThumbNailViewerExtra";
            this.Size = new System.Drawing.Size(300, 300);
            this.cmsThumbControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem tmiDeleteImage;
        private System.Windows.Forms.ContextMenuStrip cmsThumbControl;

    }
}
