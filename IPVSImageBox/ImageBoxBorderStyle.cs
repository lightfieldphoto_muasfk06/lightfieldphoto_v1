﻿//namespace Cyotek.Windows.Forms
namespace IPVS.IPVSImageBox
{
    public enum ImageBoxBorderStyle
    {
        None,
        FixedSingle,
        FixedSingleDropShadow
    }
}