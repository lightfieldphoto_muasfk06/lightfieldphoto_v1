﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//namespace Cyotek.Windows.Forms
namespace IPVS.IPVSImageBox
{
    //[TypeConverter(typeof(ZoomLevelCollectionConverter))]
    public class ZoomLevelCollection
      : IList<float>
    {

        public ZoomLevelCollection()
        {
            this.List = new SortedList<float, float>();
        }

        public ZoomLevelCollection(IList<float> collection)
            : this()
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            this.AddRange(collection);
        }

        public static ZoomLevelCollection Default
        { get { return new ZoomLevelCollection(new 
            float[] { 
            7, 
            10, 
            15, 
            20, 
            25, 
            30, 
            50, 
            70, 
            100, 
            150, 
            200, 
            300, 
            400, 
            500, 
            600, 
            700, 
            800, 
            1200, 
            1600,
            1800, 
            2000, 
            2200, 
            2400, 
            2600, 
            2800, 
            3000, 
            3200}); } }


        public int Count
        { get { return this.List.Count; } }

        public bool IsReadOnly
        { get { return false; } }

        protected SortedList<float, float> List { get; set; }

        public float this[int index]
        {
            get { return this.List.Values[index]; }
            set
            {
                this.List.RemoveAt(index);
                this.Add(value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Add(float item)
        {
            this.List.Add(item, item);
        }

        public void AddRange(IList<float> collection)
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            foreach (int value in collection)
                this.Add(value);
        }

        public void Clear()
        {
            this.List.Clear();
        }

        public bool Contains(float item)
        {
            return this.List.ContainsKey(item);
        }

        public void CopyTo(float[] array, int arrayIndex)
        {
            if (this.Count != 0)
                Array.Copy(this.List.Values.ToArray(), 0, array, arrayIndex, this.Count);
        }

        public float FindNearest(float zoomLevel)
        {
            return this.OrderBy(v => Math.Abs(v - zoomLevel)).First();
        }

        public IEnumerator<float> GetEnumerator()
        {
            return this.List.Values.GetEnumerator();
        }

        public int IndexOf(float item)
        {
            return this.List.IndexOfKey(item);
        }

        public void Insert(int index, float item)
        {
            throw new NotImplementedException();
        }

        public float NextZoom(float zoomLevel)
        {
            int index;

            index = this.IndexOf(this.FindNearest(zoomLevel));
            if (index < this.Count - 1)
                index++;

            return this[index];
        }

        public float PreviousZoom(float zoomLevel)
        {
            int index;

            index = this.IndexOf(this.FindNearest(zoomLevel));
            if (index > 0)
                index--;

            return this[index];
        }

        public bool Remove(float item)
        {
            return this.List.Remove(item);
        }

        public void RemoveAt(int index)
        {
            this.List.RemoveAt(index);
        }

        public float[] ToArray()
        {
            float[] results;

            results = new float[this.Count];
            this.CopyTo(results, 0);

            return results;
        }
    }
}