﻿

////////////////////////////////////////////////
// IPV# - Image Processing Vision # 
// Framework and Software
////////////////////////////////////////////////
// Copyright © Athanassios Alexiou, 2012-2013
// e-mail: thanassis.alex@gmail.com
////////////////////////////////////////////////
// Developed @ 
// Munich University of Applied Sciences
////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using FreeImageAPI;

using IPVS.IPVSEnumerations;

namespace IPVS.IPVSBaseOpsClasses
{
    /// <summary>
    /// Helper class for copying, loading, converting and transforming images and image related data.
    /// </summary>
    public static class CImageUtility
    {


        #region interOp unmanaged image operations

        /// <summary>
        /// Win32 - Deletes the object.
        /// </summary>
        /// <param name="hObject">The handle to an arbitrary object.</param>
        /// <returns><c>true</c> on successfull operation.</returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        /// <summary>
        /// Win32 - Gets the DC.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <returns>Device context.</returns>
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        /// <summary>
        /// Win32 - Creates the compatible DC.
        /// </summary>
        /// <param name="hdc">The HDC.</param>
        /// <returns>Device context.</returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        /// <summary>
        /// Win32 - Releases the DC.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <param name="hdc">The HDC.</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int ReleaseDC(IntPtr hwnd, IntPtr hdc);

        /// <summary>
        /// Win32 - Deletes the DC.
        /// </summary>
        /// <param name="hdc">The HDC.</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern int DeleteDC(IntPtr hdc);

        /// <summary>
        /// Win32 - Selects the object.
        /// </summary>
        /// <param name="hdc">The HDC.</param>
        /// <param name="hgdiobj">The hgdiobj.</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);




        //BOOL BitBlt(
        //              __in  HDC hdcDest,
        //              __in  int nXDest,
        //              __in  int nYDest,
        //              __in  int nWidth,
        //              __in  int nHeight,
        //              __in  HDC hdcSrc,
        //              __in  int nXSrc,
        //              __in  int nYSrc,
        //              __in  DWORD dwRop
        //            );


        //              __in  DWORD dwRop

        //Value	
        //    Meaning

        //BLACKNESS
        //    Fills the destination rectangle using the color associated with index 0 in the physical palette. (This color is black for the default physical palette.)

        //CAPTUREBLT
        //    Includes any windows that are layered on top of your window in the resulting image. By default, the image only contains your window. Note that this generally 
        //cannot be used for printing device contexts.

        //DSTINVERT
        //    Inverts the destination rectangle.

        //MERGECOPY
        //    Merges the colors of the source rectangle with the brush currently selected in hdcDest, by using the Boolean AND operator.

        //MERGEPAINT
        //    Merges the colors of the inverted source rectangle with the colors of the destination rectangle by using the Boolean OR operator.

        //NOMIRRORBITMAP
        //    Prevents the bitmap from being mirrored.

        //NOTSRCCOPY
        //    Copies the inverted source rectangle to the destination.

        //NOTSRCERASE
        //    Combines the colors of the source and destination rectangles by using the Boolean OR operator and then inverts the resultant color.

        //PATCOPY
        //    Copies the brush currently selected in hdcDest, into the destination bitmap.

        //PATINVERT
        //    Combines the colors of the brush currently selected in hdcDest, with the colors of the destination rectangle by using the Boolean XOR operator.

        //PATPAINT
        //    Combines the colors of the brush currently selected in hdcDest, with the colors of the inverted source rectangle by using the Boolean OR operator. The result of this operation is combined with the colors of the destination rectangle by using the Boolean OR operator.

        //SRCAND
        //    Combines the colors of the source and destination rectangles by using the Boolean AND operator.

        //SRCCOPY
        //    Copies the source rectangle directly to the destination rectangle.

        //SRCERASE
        //    Combines the inverted colors of the destination rectangle with the colors of the source rectangle by using the Boolean AND operator.

        //SRCINVERT
        //    Combines the colors of the source and destination rectangles by using the Boolean XOR operator.

        //SRCPAINT
        //    Combines the colors of the source and destination rectangles by using the Boolean OR operator.

        //WHITENESS
        //    Fills the destination rectangle using the color associated with index 1 in the physical palette. (This color is white for the default physical palette.)




        /// <summary>
        /// Win32 -  BitBlt method.
        /// </summary>
        /// <param name="hdcDst">The HDC DST.</param>
        /// <param name="xDst">The x DST.</param>
        /// <param name="yDst">The y DST.</param>
        /// <param name="w">The w.</param>
        /// <param name="h">The h.</param>
        /// <param name="hdcSrc">The HDC SRC.</param>
        /// <param name="xSrc">The x SRC.</param>
        /// <param name="ySrc">The y SRC.</param>
        /// <param name="rop">The rop.</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern int BitBlt(IntPtr hdcDst, int xDst, int yDst, int w, int h, IntPtr hdcSrc, int xSrc, int ySrc, int rop);
        static int SRCCOPY = 0x00CC0020;

        //HANDLE WINAPI LoadImage(
        //  _In_opt_  HINSTANCE hinst,
        //  _In_      LPCTSTR lpszName,
        //  _In_      UINT uType,
        //  _In_      int cxDesired,
        //  _In_      int cyDesired,
        //  _In_      UINT fuLoad
        //);

        //Parameters

        //hinst [in, optional]

        //    Type: HINSTANCE

        //    A handle to the module of either a DLL or executable (.exe) that contains the image to be loaded. For more information, see GetModuleHandle. Note that as of 32-bit Windows, an instance handle (HINSTANCE), such as the application instance handle exposed by system function call of WinMain, and a module handle (HMODULE) are the same thing.

        //    To load an OEM image, set this parameter to NULL.

        //    To load a stand-alone resource (icon, cursor, or bitmap file)—for example, c:\myimage.bmp—set this parameter to NULL.

        //lpszName [in]

        //    Type: LPCTSTR

        // The image to be loaded. If the hinst parameter is non-NULL and the fuLoad parameter omits LR_LOADFROMFILE, 
        // lpszName specifies the image resource in the hinst module. If the image resource is to be loaded by name from the module, the lpszName parameter is a pointer to a 
        // null-terminated string that contains the name of the image resource. If the image resource is to be loaded by ordinal from the module, 
        // use the MAKEINTRESOURCE macro to convert the image ordinal into a form that can be passed to the LoadImage function. For more information, see the Remarks section below.

        //    If the hinst parameter is NULL and the fuLoad parameter omits the LR_LOADFROMFILE value, the lpszName specifies the OEM image to load. The OEM image identifiers 
        //    are defined in Winuser.h and have the following prefixes.
        //    Prefix	Meaning
        //    OBM_	OEM bitmaps
        //    OIC_	OEM icons
        //    OCR_	OEM cursors



        //    To pass these constants to the LoadImage function, use the MAKEINTRESOURCE macro. For example, to load the OCR_NORMAL cursor, 
        // pass MAKEINTRESOURCE(OCR_NORMAL) as the lpszName parameter, NULL as the hinst parameter, and LR_SHARED as one of the flags to the fuLoad parameter.

        //    If the fuLoad parameter includes the LR_LOADFROMFILE value, lpszName is the name of the file that contains the stand-alone resource 
        // (icon, cursor, or bitmap file). Therefore, set hinst to NULL.

        //uType [in]

        //    Type: UINT

        //    The type of image to be loaded. This parameter can be one of the following values.
        //    Value	Meaning


        //    IMAGE_BITMAP
        //    0
        //    Loads a bitmap.



        //    IMAGE_CURSOR
        //    2
        //    Loads a cursor.



        //    IMAGE_ICON
        //    1
        //    Loads an icon.


        //cxDesired [in]

        //    Type: int

        //    The width, in pixels, of the icon or cursor. If this parameter is zero and the fuLoad parameter is LR_DEFAULTSIZE, 
        // the function uses the SM_CXICON or SM_CXCURSOR system metric value to set the width. If this parameter is zero and LR_DEFAULTSIZE 
        // is not used, the function uses the actual resource width.

        //cyDesired [in]

        //    Type: int

        //    The height, in pixels, of the icon or cursor. If this parameter is zero and the fuLoad parameter is 
        // LR_DEFAULTSIZE, the function uses the SM_CYICON or SM_CYCURSOR system metric value to set the height. 
        // If this parameter is zero and LR_DEFAULTSIZE is not used, the function uses the actual resource height.
        //fuLoad [in]

        //    Type: UINT

        //    This parameter can be one or more of the following values.
        //    Value	Meaning

        //    LR_CREATEDIBSECTION
        //    0x00002000



        //    When the uType parameter specifies IMAGE_BITMAP, causes the function to return a 
        // DIB section bitmap rather than a compatible bitmap. This flag is useful for loading a bitmap without 
        // mapping it to the colors of the display device.

        //    LR_DEFAULTCOLOR
        //    0x00000000



        //    The default flag; it does nothing. All it means is "not LR_MONOCHROME".

        //    LR_DEFAULTSIZE
        //    0x00000040



        //    Uses the width or height specified by the system metric values for cursors or icons, 
        // if the cxDesired or cyDesired values are set to zero. If this flag is not specified and cxDesired and cyDesired 
        // are set to zero, the function uses the actual resource size. If the resource contains multiple images, the function uses the size of the first image.

        //    LR_LOADFROMFILE
        //    0x00000010



        //    Loads the stand-alone image from the file specified by lpszName (icon, cursor, or bitmap file).

        //    LR_LOADMAP3DCOLORS
        //    0x00001000



        //    Searches the color table for the image and replaces the following shades of gray with the corresponding 3-D color.

        //        Dk Gray, RGB(128,128,128) with COLOR_3DSHADOW
        //        Gray, RGB(192,192,192) with COLOR_3DFACE
        //        Lt Gray, RGB(223,223,223) with COLOR_3DLIGHT

        //    Do not use this option if you are loading a bitmap with a color depth greater than 8bpp.

        //    LR_LOADTRANSPARENT
        //    0x00000020



        //    Retrieves the color value of the first pixel in the image and replaces the corresponding entry in the 
        // color table with the default window color (COLOR_WINDOW). All pixels in the image that use that entry become the default 
        // window color. This value applies only to images that have corresponding color tables.

        //    Do not use this option if you are loading a bitmap with a color depth greater than 8bpp.

        //    If fuLoad includes both the LR_LOADTRANSPARENT and LR_LOADMAP3DCOLORS values, LR_LOADTRANSPARENT takes precedence. 
        // However, the color table entry is replaced with COLOR_3DFACE rather than COLOR_WINDOW.

        //    LR_MONOCHROME
        //    0x00000001



        //    Loads the image in black and white.

        //    LR_SHARED
        //    0x00008000



        //    Shares the image handle if the image is loaded multiple times. If LR_SHARED is not set, a second call to 
        // LoadImage for the same resource will load the image again and return a different handle.

        //    When you use this flag, the system will destroy the resource when it is no longer needed.

        //    Do not use LR_SHARED for images that have non-standard sizes, that may change after loading, or that are loaded from a file.

        //    When loading a system icon or cursor, you must use LR_SHARED or the function will fail to load the resource.

        //    This function finds the first image in the cache with the requested resource name, regardless of the size requested.

        //    LR_VGACOLOR
        //    0x00000080



        //    Uses true VGA colors.



        //Return value

        //Type: HANDLE

        //If the function succeeds, the return value is the handle of the newly loaded image.

        //If the function fails, the return value is NULL. To get extended error information, call GetLastError.
        //Remarks

        //If IS_INTRESOURCE(lpszName) is TRUE, then lpszName specifies the integer identifier of the given resource. 
        // Otherwise, it is a pointer to a null- terminated string. If the first character of the string is a pound sign (#), 
        // then the remaining characters represent a decimal number that specifies the integer identifier of the resource. 
        // For example, the string "#258" represents the identifier 258.

        //When you are finished using a bitmap, cursor, or icon you loaded without specifying the LR_SHARED flag, 
        // you can release its associated memory by calling one of the functions in the following table.

        //Resource	Release function
        //Bitmap	DeleteObject
        //Cursor	DestroyCursor
        //Icon	DestroyIcon



        //   The system automatically deletes these resources when the process that created them terminates; 
        // however, calling the appropriate function saves memory and decreases the size of the process's working set.
        // Examples

        //For an example, see Using Window Classes.
        //Requirements

        //Minimum supported client
        //    Windows 2000 Professional

        //Minimum supported server
        //    Windows 2000 Server

        //Header
        //Winuser.h (include Windows.h)

        //Library
        //User32.lib

        //DLL
        //User32.dll

        //Unicode and ANSI names
        //    LoadImageW (Unicode) and LoadImageA (ANSI)



        //        HANDLE WINAPI LoadImage(
        //  _In_opt_  HINSTANCE hinst,
        //  _In_      LPCTSTR lpszName,
        //  _In_      UINT uType,
        //  _In_      int cxDesired,
        //  _In_      int cyDesired,
        //  _In_      UINT fuLoad
        //);



        //        HBITMAP LoadBitmap(
        //  __in  HINSTANCE hInstance,
        //  __in  LPCTSTR lpBitmapName
        //);


        /// <summary>
        /// Win32 - Loads the image.
        /// </summary>
        /// <param name="hInstance">The h instance.</param>
        /// <param name="lpszName">Name of the LPSZ.</param>
        /// <param name="type">The type.</param>
        /// <param name="cxDesired">The cx desired.</param>
        /// <param name="cyDesired">The cy desired.</param>
        /// <param name="fuLoad">The fu load.</param>
        /// <returns>Handle to a bitmap object.</returns>
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr LoadImage(IntPtr hInstance, String lpszName, uint type, int cxDesired, int cyDesired, uint fuLoad);

        /// <summary>
        /// Win32 - Loads the bitmap.
        /// </summary>
        /// <param name="hInstance">The h instance.</param>
        /// <param name="lpBitmapName">Name of the lp bitmap.</param>
        /// <returns>Handle to a GDI bitmap object.</returns>
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern IntPtr LoadBitmap(IntPtr hInstance, String lpBitmapName);

        //HBITMAP bitmap = (HBITMAP)LoadImage(NULL, "bitmap2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);


        ///// <summary>
        ///// DO NOT USE - Generic GDI+ Error  ?????????????
        ///// </summary>
        ///// <param name="Filename"></param>
        ///// <returns></returns>
        //public static System.Drawing.Bitmap loadImageWin32(System.String Filename)
        //{
        //    uint IMAGE_BITMAP = 0;
        //    uint LR_LOADFROMFILE = 0x00000010;
        //    uint LR_LOADMAP3DCOLORS = 0x00001000;
        //    uint LR_CREATEDIBSECTION = 0x00002000;

        //    System.Drawing.Bitmap fromWin32Bmp = null;

        //    IntPtr HBitmap = IntPtr.Zero;

        //    try
        //    {

        //        //IntPtr HBitmap = LoadImage(IntPtr.Zero, Filename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION );
        //        HBitmap = LoadImage(IntPtr.Zero, Filename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

        //        fromWin32Bmp = Bitmap.FromHbitmap(HBitmap, IntPtr.Zero);

        //    }
        //    catch
        //    {

        //        System.String error = GetLastErrorMessage();

        //        error += "!";

        //    }
        //    finally
        //    {

        //        DeleteObject(HBitmap);

        //    }



        //    //fromWin32Bmp = new Bitmap(Filename);

        //    return fromWin32Bmp;
        //}




        //        HBITMAP CreateBitmap(
        //  __in  int nWidth,
        //  __in  int nHeight,
        //  __in  UINT cPlanes,
        //  __in  UINT cBitsPerPel,
        //  __in  const VOID *lpvBits
        //);




        /// <summary>
        /// Win32 - Creates the bitmap.
        /// </summary>
        /// <param name="nWidth">Width of the n.</param>
        /// <param name="nHeight">Height of the n.</param>
        /// <param name="cPlanes">The c planes.</param>
        /// <param name="cBitsPerPel">The c bits per pel.</param>
        /// <param name="lpvBits">The LPV bits.</param>
        /// <returns>Handle to a GDI bitmap object.</returns>
        [System.Runtime.InteropServices.DllImport("Gdi32.dll")]
        public static extern IntPtr CreateBitmap(int nWidth, int nHeight, uint cPlanes, uint cBitsPerPel, ref IntPtr lpvBits);

        //        Parameters

        //nWidth [in]

        //    The bitmap width, in pixels.
        //nHeight [in]

        //    The bitmap height, in pixels.
        //cPlanes [in]

        //    The number of color planes used by the device.
        //cBitsPerPel [in]

        //    The number of bits required to identify the color of a single pixel.
        //lpvBits [in]

        //    A pointer to an array of color data used to set the colors in a rectangle of pixels. Each scan line in the rectangle must be word aligned (scan lines that are not word aligned must be padded with zeros). If this parameter is NULL, the contents of the new bitmap is undefined.





        /// <summary>
        /// Win32 - Gets the last error.
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetLastError();

        /// <summary>
        /// Win32 - Formats the message.
        /// </summary>
        /// <param name="dwFlags">The dw flags.</param>
        /// <param name="lpSource">The lp source.</param>
        /// <param name="dwMessageId">The dw message id.</param>
        /// <param name="dwLanguageId">The dw language id.</param>
        /// <param name="lpBuffer">The lp buffer.</param>
        /// <param name="nSize">Size of the n.</param>
        /// <param name="Arguments">The arguments.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int FormatMessage(int dwFlags,
            string lpSource,
            int dwMessageId,
            int dwLanguageId,
            StringBuilder lpBuffer,
            int nSize,
            string[] Arguments);


        /// <summary>
        /// .Net from Win32 - Gets the last error message.
        /// </summary>
        /// <returns>Error message.</returns>
        public static string GetLastErrorMessage()
        {
            StringBuilder strLastErrorMessage = new StringBuilder(255);
            int ret2 = GetLastError();
            int dwFlags = 4096;

            int ret3 = FormatMessage(dwFlags,
                null,
                ret2,
                0,
                strLastErrorMessage,
                strLastErrorMessage.Capacity,
                null);

            return strLastErrorMessage.ToString();
        }




        /// <summary>
        /// Win32 - Creates the DIB section.
        /// </summary>
        /// <param name="hdc">The HDC.</param>
        /// <param name="bmi">The bmi.</param>
        /// <param name="Usage">The usage.</param>
        /// <param name="bits">The bits.</param>
        /// <param name="hSection">The h section.</param>
        /// <param name="dwOffset">The dw offset.</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        static extern IntPtr CreateDIBSection(IntPtr hdc, ref BITMAPINFO bmi, uint Usage, out IntPtr bits, IntPtr hSection, uint dwOffset);
        static uint BI_RGB = 0;
        static uint DIB_RGB_COLORS = 0;
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct BITMAPINFO
        {
            public uint biSize;
            public int biWidth, biHeight;
            public short biPlanes, biBitCount;
            public uint biCompression, biSizeImage;
            public int biXPelsPerMeter, biYPelsPerMeter;
            public uint biClrUsed, biClrImportant;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 256)]
            public uint[] cols;
        }

        /// <summary>
        /// Generates pixel values.
        /// </summary>
        /// <param name="r">The red.</param>
        /// <param name="g">The green.</param>
        /// <param name="b">The blue.</param>
        /// <returns>GDI pixel value.</returns>
        private static uint MAKERGB(int r, int g, int b)
        {
            return ((uint)(b & 255)) | ((uint)((r & 255) << 8)) | ((uint)((g & 255) << 16));
        }



        #endregion



        /// <summary>
        /// Transforms the length of an image pixel from one metric unit to another.
        /// </summary>
        /// <param name="metricLengthPerPixel">The metric length per pixel.</param>
        /// <param name="MetricXYUnit">The initial metric unit.</param>
        /// <param name="metricU">The new metric unit.</param>
        /// <returns>The transformed length in the new metric units.</returns>
        public static double TransformLengthPerPixelToNewMetricUnit(double metricLengthPerPixel, MetricUnitsEnum oldMetricUnit, MetricUnitsEnum newMetricUnit)
        {
            if (oldMetricUnit != newMetricUnit)
            {
                double power = (double)((int)oldMetricUnit - (int)newMetricUnit);
                return
                    ((metricLengthPerPixel * System.Math.Pow(10.0, power)));
            }
            else
                return
                    metricLengthPerPixel;
        }



        /// <summary>
        /// Copies a bitmap into a 1bpp/4bpp/8bpp bitmap of the same dimensions, fast
        /// </summary>
        /// <param name="b">original bitmap</param>
        /// <param name="bpp">1, 4 or 8, target bpp</param>
        /// <param name="palette">The palette.</param>
        /// <returns>
        /// A 1, 4 or 8 bpp copy of the bitmap
        /// </returns>
        public static System.Drawing.Bitmap CopyToIndexedBpp(System.Drawing.Bitmap b, int bpp, uint[] palette)
        {

            if (bpp != 1 && bpp != 4 && bpp != 8)
            {//throw new System.ArgumentException("1, 4 or 8", "bpp");
                return b;
            }
            // Plan: built into Windows GDI is the ability to convert
            // bitmaps from one format to another. Most of the time, this
            // job is actually done by the graphics hardware accelerator card
            // and so is extremely fast. The rest of the time, the job is done by
            // very fast native code.
            // We will call into this GDI functionality from C#. Our plan:
            // (1) Convert our Bitmap into a GDI hbitmap (ie. copy unmanaged->managed)
            // (2) Create a GDI monochrome hbitmap
            // (3) Use GDI "BitBlt" function to copy from hbitmap into monochrome (as above)
            // (4) Convert the monochrone hbitmap into a Bitmap (ie. copy unmanaged->managed) 

            int w = b.Width, h = b.Height;
            IntPtr hbm = b.GetHbitmap(); // this is step (1)
            //
            // Step (2): create the monochrome bitmap.
            // "BITMAPINFO" is an interop-struct which we define below.
            // In GDI terms, it's a BITMAPHEADERINFO followed by an array of two RGBQUADs
            BITMAPINFO bmi = new BITMAPINFO();
            bmi.biSize = 40;  // the size of the BITMAPHEADERINFO struct
            bmi.biWidth = w;
            bmi.biHeight = h;
            bmi.biPlanes = 1; // "planes" are confusing. We always use just 1. Read MSDN for more info.
            bmi.biBitCount = (short)bpp; // ie. 1bpp,, 4bpp or 8bpp
            bmi.biCompression = BI_RGB; // ie. the pixels in our RGBQUAD table are stored as RGBs, not palette indexes
            bmi.biSizeImage = (uint)(((w + 7) & 0xFFFFFFF8) * h / 8);
            bmi.biXPelsPerMeter = 1000000; // not really important
            bmi.biYPelsPerMeter = 1000000; // not really important
            // Now for the colour table.
            uint ncols = (uint)1 << bpp; // 2 colours for 1bpp; 16 for 4bpp; 256 colours for 8bpp
            bmi.biClrUsed = ncols;
            bmi.biClrImportant = ncols;

            bmi.cols = new uint[256]; // The structure always has fixed size 256, even if we end up using fewer colours

            if (palette.Length == 256)
            {
                //for (int i = 0; i < 256; i++)
                //{
                //    bmi.cols[i] = palette[i];
                //}


                bmi.cols = palette;

            }
            else
            {

                bmi.cols = new uint[256]; // The structure always has fixed size 256, even if we end up using fewer colours

                if (bpp == 1)
                {
                    bmi.cols[0] = MAKERGB(0, 0, 0); bmi.cols[1] = MAKERGB(255, 255, 255);
                }
                else if (bpp == 4)
                {
                    int greyscale = 0;
                    for (int i = 0; i < ncols; i++)
                    {
                        
                        bmi.cols[i] = MAKERGB(greyscale, greyscale, greyscale);
                        greyscale = (i * 16) - 1;

                    }
                }
                else if (bpp == 8)
                {
                    for (int i = 0; i < ncols; i++) bmi.cols[i] = MAKERGB(i, i, i);
                }

            }
            // For 8bpp we've created an palette with just greyscale colours.
            // You can set up any palette you want here. Here are some possibilities:
            // greyscale: for (int i=0; i<256; i++) bmi.cols[i]=MAKERGB(i,i,i);
            // rainbow: bmi.biClrUsed=216; bmi.biClrImportant=216; int[] colv=new int[6]{0,51,102,153,204,255};
            //          for (int i=0; i<216; i++) bmi.cols[i]=MAKERGB(colv[i/36],colv[(i/6)%6],colv[i%6]);
            // optimal: a difficult topic: http://en.wikipedia.org/wiki/Color_quantization
            // 
            // Now create the indexed bitmap "hbm0"
            IntPtr bits0; // not used for our purposes. It returns a pointer to the raw bits that make up the bitmap.
            IntPtr hbm0 = CreateDIBSection(IntPtr.Zero, ref bmi, DIB_RGB_COLORS, out bits0, IntPtr.Zero, 0);
            //
            // Step (3): use GDI's BitBlt function to copy from original hbitmap into monocrhome bitmap
            // GDI programming is kind of confusing... nb. The GDI equivalent of "Graphics" is called a "DC".
            IntPtr sdc = GetDC(IntPtr.Zero);       // First we obtain the DC for the screen
            // Next, create a DC for the original hbitmap
            IntPtr hdc = CreateCompatibleDC(sdc); SelectObject(hdc, hbm);
            // and create a DC for the monochrome hbitmap
            IntPtr hdc0 = CreateCompatibleDC(sdc); SelectObject(hdc0, hbm0);
            // Now we can do the BitBlt:
            BitBlt(hdc0, 0, 0, w, h, hdc, 0, 0, SRCCOPY);
            // Step (4): convert this monochrome hbitmap back into a Bitmap:
            System.Drawing.Bitmap b0 = System.Drawing.Bitmap.FromHbitmap(hbm0);
            //
            // Finally some cleanup.
            DeleteDC(hdc);
            DeleteDC(hdc0);
            ReleaseDC(IntPtr.Zero, sdc);
            DeleteObject(hbm);
            DeleteObject(hbm0);
            //
            return b0;
        }



        /// <summary>
        /// Copies a bitmap into a 1bpp/8bpp bitmap of the same dimensions, fast
        /// </summary>
        /// <param name="b">original bitmap</param>
        /// <param name="bpp">1, 4 or 8, target bpp</param>
        /// <returns>a 1bpp copy of the bitmap</returns>
        public static System.Drawing.Bitmap CopyToIndexedBpp(System.Drawing.Bitmap b, int bpp)
        {
            if (bpp != 1 && bpp != 8 && bpp != 4)
            {//throw new System.ArgumentException("1, 4 or 8", "bpp");
                return b;
            }
            // Plan: built into Windows GDI is the ability to convert
            // bitmaps from one format to another. Most of the time, this
            // job is actually done by the graphics hardware accelerator card
            // and so is extremely fast. The rest of the time, the job is done by
            // very fast native code.
            // We will call into this GDI functionality from C#. Our plan:
            // (1) Convert our Bitmap into a GDI hbitmap (ie. copy unmanaged->managed)
            // (2) Create a GDI monochrome hbitmap
            // (3) Use GDI "BitBlt" function to copy from hbitmap into monochrome (as above)
            // (4) Convert the monochrone hbitmap into a Bitmap (ie. copy unmanaged->managed)

            int w = b.Width, h = b.Height;
            IntPtr hbm = b.GetHbitmap(); // this is step (1)
            //
            // Step (2): create the monochrome bitmap.
            // "BITMAPINFO" is an interop-struct which we define below.
            // In GDI terms, it's a BITMAPHEADERINFO followed by an array of two RGBQUADs
            BITMAPINFO bmi = new BITMAPINFO();
            bmi.biSize = 40;  // the size of the BITMAPHEADERINFO struct
            bmi.biWidth = w;
            bmi.biHeight = h;
            bmi.biPlanes = 1; // "planes" are confusing. We always use just 1. Read MSDN for more info.
            bmi.biBitCount = (short)bpp; // ie. 1bpp or 8bpp
            bmi.biCompression = BI_RGB; // ie. the pixels in our RGBQUAD table are stored as RGBs, not palette indexes
            bmi.biSizeImage = (uint)(((w + 7) & 0xFFFFFFF8) * h / 8);
            bmi.biXPelsPerMeter = 1000000; // not really important
            bmi.biYPelsPerMeter = 1000000; // not really important
            // Now for the colour table.
            uint ncols = (uint)1 << bpp; // 2 colours for 1bpp; 256 colours for 8bpp
            bmi.biClrUsed = ncols;
            bmi.biClrImportant = ncols;
            bmi.cols = new uint[256]; // The structure always has fixed size 256, even if we end up using fewer colours
            if (bpp == 1)
            {
                bmi.cols[0] = MAKERGB(0, 0, 0); bmi.cols[1] = MAKERGB(255, 255, 255);
            }
            else if (bpp == 4)
            {
                int greyscale = 0;

                for (int i = 0; i < ncols; i++)
                {
                    bmi.cols[i] = MAKERGB(greyscale, greyscale, greyscale);
                    greyscale = (i * 16) - 1;
                }
            }
            else if (bpp == 8)
            {
                for (int i = 0; i < ncols; i++) bmi.cols[i] = MAKERGB(i, i, i);
            }
            // For 8bpp we've created an palette with just greyscale colours.
            // You can set up any palette you want here. Here are some possibilities:
            // greyscale: for (int i=0; i<256; i++) bmi.cols[i]=MAKERGB(i,i,i);
            // rainbow: bmi.biClrUsed=216; bmi.biClrImportant=216; int[] colv=new int[6]{0,51,102,153,204,255};
            //          for (int i=0; i<216; i++) bmi.cols[i]=MAKERGB(colv[i/36],colv[(i/6)%6],colv[i%6]);
            // optimal: a difficult topic: http://en.wikipedia.org/wiki/Color_quantization
            // 
            // Now create the indexed bitmap "hbm0"
            IntPtr bits0; // not used for our purposes. It returns a pointer to the raw bits that make up the bitmap.
            IntPtr hbm0 = CreateDIBSection(IntPtr.Zero, ref bmi, DIB_RGB_COLORS, out bits0, IntPtr.Zero, 0);
            //
            // Step (3): use GDI's BitBlt function to copy from original hbitmap into monocrhome bitmap
            // GDI programming is kind of confusing... nb. The GDI equivalent of "Graphics" is called a "DC".
            IntPtr sdc = GetDC(IntPtr.Zero);       // First we obtain the DC for the screen
            // Next, create a DC for the original hbitmap
            IntPtr hdc = CreateCompatibleDC(sdc); SelectObject(hdc, hbm);
            // and create a DC for the monochrome hbitmap
            IntPtr hdc0 = CreateCompatibleDC(sdc); SelectObject(hdc0, hbm0);
            // Now we can do the BitBlt:
            BitBlt(hdc0, 0, 0, w, h, hdc, 0, 0, SRCCOPY);
            // Step (4): convert this monochrome hbitmap back into a Bitmap:
            System.Drawing.Bitmap b0 = System.Drawing.Bitmap.FromHbitmap(hbm0);
            //
            // Finally some cleanup.
            DeleteDC(hdc);
            DeleteDC(hdc0);
            ReleaseDC(IntPtr.Zero, sdc);
            DeleteObject(hbm);
            DeleteObject(hbm0);
            //
            return b0;
        }



        /// <summary>
        /// Generates a greyscale palette with aequidistante greyscales
        /// </summary>
        /// <param name="numOfGreyscales">Number of greyscales</param>
        /// <returns>uint[]: Returns the palette</returns>
        public static uint[] generate8bppGreyScalePalette(int numOfGreyscales)
        {

            int greyscale = 0;

            uint[] palette = new uint[256];

            palette[0] = 0;


            if ((numOfGreyscales) <= 256)
            {
                double factor = 255.0 / ((double)(numOfGreyscales - 1));

                for (int i = 1; i < (numOfGreyscales - 1); i++)
                {
                    greyscale = (int)System.Math.Round((double)i * factor);

                    palette[i] = MAKERGB(greyscale, greyscale, greyscale);

                }


                greyscale = 255;
                palette[numOfGreyscales - 1] = MAKERGB(greyscale, greyscale, greyscale);

            }
            else
            {

                for (int i = 0; i < (256); i++)
                {
                    greyscale = i;

                    palette[i] = MAKERGB(greyscale, greyscale, greyscale);

                }
            }


            return palette;
        }




        /// <summary>
        /// Generates a LUT for transfering 16 Bit color scales to 8 Bit colors!!
        /// </summary>
        /// <returns>byte[]: Returns the palette</returns>
        public static byte[] generate16BitTo8BitLUT()
        {
            byte[] LUT16To8 = new byte[65536];

            for (int i = 0; i < 65536; i++)
            {
                LUT16To8[i] = (byte)(i / 256);
            }

            return LUT16To8;
        }




        /// <summary>
        /// Generates a LUT for transfering 8 Bit color scales to 16 Bit colors!!
        /// </summary>
        /// <returns>byte[]: Returns the palette</returns>
        public static int[] generate8BitTo16BitLUT()
        {
            int[] LUT8To16 = new int[256];

            for (int i = 0; i < 256; i++)
            {
                LUT8To16[i] = (int)(i * 256);
            }

            return LUT8To16;
        }




        /// <summary>
        /// Returns a normalized double matrix of an image channel.
        /// </summary>
        /// <param name="inBmp">Input bitmap</param>
        /// <param name="channel">Bitmap color channel: 0 = blue, 1 = green, 2 = red, 3 = alpha</param>
        /// <returns>Double matrix</returns>
        public static unsafe double[,] bitmapToNormalizedMatrix64f(System.Drawing.Bitmap inBmp, int channel)
        {


            int allPixels = inBmp.Width * inBmp.Height, w = inBmp.Width, h = inBmp.Height;

            double[,] returnMatrix = new double[w, h];


            double denominator = 1;

            switch (inBmp.PixelFormat)
            {
                case PixelFormat.Format16bppGrayScale
                    //| PixelFormat.Format48bppRgb
                    //| PixelFormat.Format64bppPArgb 
                    | PixelFormat.Format64bppArgb:
                    {
                        denominator = System.Math.Pow(2, 16) - 1.0;
                        break;
                    }
                case PixelFormat.Format32bppArgb
                    //| PixelFormat.Format4bppIndexed 
                    //| PixelFormat.Format1bppIndexed
                    //| PixelFormat.Format32bppPArgb
                    //| PixelFormat.Format16bppArgb1555
                    //| PixelFormat.Format16bppRgb555
                    //| PixelFormat.Format16bppRgb565
                    //| PixelFormat.Format24bppRgb
                    //| PixelFormat.Format8bppIndexed
                    :
                    {
                        denominator = System.Math.Pow(2, 8) - 1.0;
                        break;
                    }
            }




            int* ptrAllPx = &allPixels,
                ptrWidth = &w,
                ptrHeight = &h,
                ptrChannel = &channel;


            fixed (double* ptrMatrix = returnMatrix)
            {

                // The Input image is taken and his BitmaData object is being created
                System.Drawing.Rectangle inImgRect = new System.Drawing.Rectangle(0, 0, inBmp.Width, inBmp.Height);

                System.Drawing.Imaging.BitmapData bmpDataIn = inBmp.LockBits(inImgRect, ImageLockMode.ReadWrite, inBmp.PixelFormat);
                byte* ptr = (byte*)bmpDataIn.Scan0;
                double* ptrMtx = ptrMatrix;

                for (int i = 0; i < (*ptrHeight); i++)
                {
                    for (int j = 0; j < (*ptrWidth); j++)
                    {

                        *ptrMtx = (((double)ptr[*ptrChannel]) / denominator);

                        ptrMtx++;
                    }
                }


            }

            return returnMatrix;

        }


        /// <summary>
        /// Returns a normalized float matrix of an image channel.
        /// </summary>
        /// <param name="inBmp">Input bitmap</param>
        /// <param name="channel">Bitmap color channel: 0 = blue, 1 = green, 2 = red, 3 = alpha</param>
        /// <returns>float: matrix</returns>
        public static unsafe float[,] bitmapToNormalizedMatrix32f(System.Drawing.Bitmap inBmp, int channel)
        {


            int allPixels = inBmp.Width * inBmp.Height, w = inBmp.Width, h = inBmp.Height;

            float[,] returnMatrix = new float[w, h];


            float denominator = 1;

            switch (inBmp.PixelFormat)
            {
                case PixelFormat.Format16bppGrayScale
                    //| PixelFormat.Format48bppRgb
                    //| PixelFormat.Format64bppPArgb 
                    | PixelFormat.Format64bppArgb:
                    {
                        denominator = (float)(System.Math.Pow(2, 16) - 1.0);
                        break;
                    }
                case PixelFormat.Format32bppArgb
                    //| PixelFormat.Format4bppIndexed 
                    //| PixelFormat.Format1bppIndexed
                    //| PixelFormat.Format32bppPArgb
                    //| PixelFormat.Format16bppArgb1555
                    //| PixelFormat.Format16bppRgb555
                    //| PixelFormat.Format16bppRgb565
                    //| PixelFormat.Format24bppRgb
                    //| PixelFormat.Format8bppIndexed
                    :
                    {
                        denominator = (float)(System.Math.Pow(2, 8) - 1.0);
                        break;
                    }
            }




            int* ptrAllPx = &allPixels,
                ptrWidth = &w,
                ptrHeight = &h,
                ptrChannel = &channel;



            fixed (float* ptrMatrix = returnMatrix)
            {

                // The Input image is taken and his BitmaData object is being created
                System.Drawing.Rectangle inImgRect = new System.Drawing.Rectangle(0, 0, inBmp.Width, inBmp.Height);

                System.Drawing.Imaging.BitmapData bmpDataIn = inBmp.LockBits(inImgRect, ImageLockMode.ReadWrite, inBmp.PixelFormat);
                byte* ptr = (byte*)bmpDataIn.Scan0;
                float* ptrMtx = ptrMatrix;

                for (int i = 0; i < (*ptrHeight); i++)
                {
                    for (int j = 0; j < (*ptrWidth); j++)
                    {

                        *ptrMtx = (((float)ptr[*ptrChannel]) / denominator);

                        ptrMtx++;
                    }
                }


            }

            return returnMatrix;

        }



        /// <summary>
        /// Returns a byte array from a memorystream out of an arbitrary Bitmap.
        /// </summary>
        /// <param name="inputImage">The input image.</param>
        /// <returns>
        /// byte[]: the byte array
        /// </returns>
        public static byte[] bitmapToByteArrayStream(System.Drawing.Image inputImage)
        {

            MemoryStream ms = null;

            try
            {
                ms = new MemoryStream();
                inputImage.Save(ms, ImageFormat.Bmp);
                return ms.ToArray();
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
        }



        /// <summary>
        /// Returns a Bitmap from a memorystream out of a byte array.
        /// </summary>
        /// <param name="inputByte">Input byte Array</param>
        /// <returns>Bitmap</returns>
        public static Bitmap byteArrayToBitmapStream(byte[] inputByte)
        {
            MemoryStream ms = null;

            System.Drawing.Bitmap tempBmp = null;

            try
            {
                ms = new MemoryStream(inputByte);
                tempBmp = new Bitmap(ms);
                return tempBmp;
            }
            finally
            {
                ms.Close();
                ms.Dispose();
            }
        }



        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Unmanaged copy of a byte array containing bitmap data into a bitmap object.
        /// </summary>
        /// <param name="width">Bitmap width</param>
        /// <param name="height">Bitmap height</param>
        /// <param name="pxlFormat">Bitmap pixelformat</param>
        /// <param name="data">Byte array to be copied</param>
        /// <returns><see cref="System.Drawing.Bitmap"/> object.</returns>
        public static Bitmap CopyByteArrayToBitmap(
            int width, 
            int height, 
            System.Drawing.Imaging.PixelFormat pxlFormat, 
            byte[] data)
        {
            //Here create the Bitmap to the known width, height and format
            Bitmap bmp = new Bitmap(width, height, pxlFormat);

            BitmapData bmpData = null;

            try
            {
                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.ReadWrite, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
            }
            catch
            {
                return null;

            }
            finally
            {
                data = null;
                GC.Collect();
                //Unlock the pixels
                bmp.UnlockBits(bmpData);
            }

            //Return the bitmap 
            return bmp;
        }



        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Unmanaged copy of a byte array containing bitmap data into 
        /// a <see cref="System.Drawing.Imaging.BitmapData"/> object.
        /// </summary>
        /// <param name="width">Bitmap width.</param>
        /// <param name="height">Bitmap height.</param>
        /// <param name="pxlFormat">Bitmap pixelformat.</param>
        /// <param name="data">Byte array to be copied.</param>
        /// <returns><see cref="System.Drawing.Imaging.BitmapData"/> object.</returns>
        public static BitmapData CopyByteArrayToBitmapData(
            int width, 
            int height, 
            System.Drawing.Imaging.PixelFormat pxlFormat, 
            byte[] data)
        {
            //Here create the Bitmap to the known width, height and format
            Bitmap bmp = new Bitmap(width, height, pxlFormat);

            BitmapData bmpData = null;

            try
            {
                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.ReadWrite, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
            }
            catch
            {
                return null;

            }
            finally
            {
                data = null;
                GC.Collect();
            }

            //Return the bitmap 
            return bmpData;
        }


        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Unmanaged copy of a short (Int16) array containing bitmap data into a bitmap object
        /// </summary>
        /// <param name="width">Bitmap width</param>
        /// <param name="height">Bitmap height</param>
        /// <param name="pxlFormat">Bitmap pixelformat</param>
        /// <param name="data">short array to be copied</param>
        /// <returns><see cref="System.Drawing.Bitmap"/> object.</returns>
        public static Bitmap CopyInt16ArrayToBitmap(int width, int height, System.Drawing.Imaging.PixelFormat pxlFormat, Int16[] data)
        {


            //Here create the Bitmap to the known width, height and format
            Bitmap bmp = new Bitmap(width, height, pxlFormat);

            BitmapData bmpData = null;

            try
            {


                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(data, 0, bmpData.Scan0, data.Length);


            }
            catch
            {
                return null;

            }
            finally
            {

                data = null;
                GC.Collect();
                //Unlock the pixels
                bmp.UnlockBits(bmpData);



            }


            //Return the bitmap 
            return bmp;
        }




        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Unmanaged copy of an int (Int32) array containing bitmap data into a bitmap object.
        /// </summary>
        /// <param name="width">Bitmap width</param>
        /// <param name="height">Bitmap height</param>
        /// <param name="pxlFormat">Bitmap pixelformat</param>
        /// <param name="data">int array to be copied</param>
        /// <returns><see cref="System.Drawing.Bitmap"/> object.</returns>
        public static Bitmap CopyInt32ArrayToBitmap(int width, int height, System.Drawing.Imaging.PixelFormat pxlFormat, Int32[] data)
        {
            //Here create the Bitmap to the known width, height and format
            Bitmap bmp = new Bitmap(width, height, pxlFormat);

            BitmapData bmpData = null;

            try
            {


                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
            }
            catch
            {
                return null;

            }
            finally
            {

                data = null;
                GC.Collect();
                //Unlock the pixels
                bmp.UnlockBits(bmpData);



            }


            //Return the bitmap 
            return bmp;
        }



        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed byte array
        /// </summary>
        /// <param name="bmp">Input <see cref="System.Drawing.Bitmap"/>.</param>
        /// <returns>byte[]: byte array</returns>
        public static byte[] CopyBitmapToByteArray(System.Drawing.Bitmap bmp)
        {

            if (bmp == null) return null;

            int channels = getNumOfChannels(bmp.PixelFormat);
            int byteDepth = getByteDepth(bmp.PixelFormat);
            int numOfBytes = (bmp.Width * bmp.Height) * channels * byteDepth;


            // create the byte array of specific length
            byte[] byteArray = new byte[numOfBytes];
            BitmapData bmpData = null;

            try
            {

                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);



                IntPtr getScan0Ptr = bmpData.Scan0;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);

            }
            catch
            {
                byteArray = null;
                GC.Collect();
                return null;
            }
            finally
            {
                //Unlock the pixels
                if (bmpData != null) bmp.UnlockBits(bmpData);
            }

            return byteArray;
        }




        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed byte array
        /// </summary>
        /// <param name="bmpData">The <see cref="System.Drawing.Imaging.BitmapData"/> object.</param>
        /// <returns>
        /// byte[]: byte array
        /// </returns>
        public static byte[] CopyBitmapDataToByteArray(System.Drawing.Imaging.BitmapData bmpData)
        {
            if (bmpData == null) return null;

            int channels = getNumOfChannels(bmpData.PixelFormat);
            int byteDepth = getByteDepth(bmpData.PixelFormat);
            int numOfBytes = (bmpData.Width * bmpData.Height) * channels * byteDepth;

            // create the byte array of specific length
            byte[] byteArray = new byte[numOfBytes];

            try
            {
                IntPtr getScan0Ptr = bmpData.Scan0;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);
            }
            catch
            {
                byteArray = null;
                GC.Collect();
                return null;
            }
            finally
            {

            }

            return byteArray;
        }





        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed byte array
        /// </summary>
        /// <param name="bmp">Input <see cref="System.Drawing.Bitmap" />.</param>
        /// <param name="fromPixel">From pixel number (begins with 0, ends with (width*height - 1) = last pixel).</param>
        /// <param name="numberOfPixel">The number of pixels (maximum image pixels e.g. = width*height.</param>
        /// <returns>
        /// byte[]: byte array
        /// </returns>
        public static unsafe byte[] CopyBitmapPartToByteArray(
            System.Drawing.Bitmap bmp,
            int fromPixel,
            int numberOfPixel)
        {

            if (bmp == null) return null;

            int channels = getNumOfChannels(bmp.PixelFormat);
            int byteDepth = getByteDepth(bmp.PixelFormat);
            int pixel = channels * byteDepth;

            fromPixel = (fromPixel * pixel);
            int numOfBytes = (numberOfPixel * pixel);


            // create the byte array of specific length
            byte[] byteArray = new byte[numOfBytes];
            BitmapData bmpData = null;

            try
            {

                byte* bmpPtr = (byte*)bmpData.Scan0;
                bmpPtr += fromPixel;
                IntPtr getScan0Ptr = (IntPtr)bmpPtr;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);

            }
            catch
            {
                return null;
            }
            finally
            {
                //Unlock the pixels
                if (bmpData != null) bmp.UnlockBits(bmpData);
            }

            return byteArray;
        }




        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed byte array
        /// </summary>
        /// <param name="bmpData">The <see cref="System.Drawing.Imaging.BitmapData"/> data.</param>
        /// <param name="fromPixel">From pixel number (begins with 0, ends with (width*height - 1) = last pixel).</param>
        /// <param name="numberOfPixel">The number of pixels (maximum image pixels e.g. = width*height.</param>
        /// <returns>
        /// byte[]: byte array
        /// </returns>
        public static unsafe byte[] CopyBitmapPartToByteArray(
            System.Drawing.Imaging.BitmapData bmpData, 
            int fromPixel, 
            int numberOfPixel)
        {

            if (bmpData == null) return null;

            int channels = getNumOfChannels(bmpData.PixelFormat);
            int byteDepth = getByteDepth(bmpData.PixelFormat);
            int pixel = channels * byteDepth;

            fromPixel = (fromPixel * pixel);
            int numOfBytes = (numberOfPixel * pixel);

            // create the byte array of specific length
            byte[] byteArray = new byte[numOfBytes];

            try
            {
                byte* bmpPtr = (byte*)bmpData.Scan0;
                bmpPtr += fromPixel;
                IntPtr getScan0Ptr = (IntPtr)bmpPtr;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);
            }
            catch
            {
                return null;
            }
            finally
            {

            }

            return byteArray;
        }



        /// <summary>
        /// Concatenates the byte arrays.
        /// </summary>
        /// <param name="inputArrays">The input arrays.</param>
        /// <returns></returns>
        public static unsafe byte[] ConcatenateByteArrays(byte[][] inputArrays)
        {
            byte[] result = null;
            int currentOffset = 0;

            foreach (byte[] b in inputArrays)
            {
                Buffer.BlockCopy(b, 0, result, currentOffset, b.Length);
                currentOffset += b.Length;
            }

            return result;
        }


        //public static unsafe BitmapData ConcatenateBitmapParts(
        //    System.Drawing.Imaging.BitmapData[] bmpDataArray,
        //    int width,
        //    int height,
        //    PixelFormat pixelFormat)
        //{

        //    if (bmpDataArray == null) return null;

        //    Bitmap bmpOut = new Bitmap(width, height, pixelFormat);

        //    Rectangle outRect = new Rectangle(0, 0, width, height);
        //    BitmapData bmpData = bmpOut.LockBits(outRect, ImageLockMode.ReadWrite, pixelFormat);

        //    int channels = getNumOfChannels(pixelFormat);
        //    int byteDepth = getByteDepth(pixelFormat);
        //    int pixel = channels * byteDepth;

        //    int numOfBytes = (width * height * pixel);

        //    // create the byte array of specific length
        //    byte[] byteArray = new byte[numOfBytes];

        //    //System.Threading.Tasks.Parallel.ForEach(bmpDataArray, bmpDt =>
        //    //    {
        //    //        byte[] temp = CopyBitmapDataToByteArray(bmpDt);

        //    //        //Buffer.BlockCopy(temp,
        //    //    }
        //    //);


        //    //Marshal.Copy(data, 0, bmpData.Scan0, data.Length);

        //    try
        //    {



        //        byte* bmpPtr = (byte*)bmpData.Scan0;
        //        bmpPtr += fromPixel;
        //        IntPtr getScan0Ptr = (IntPtr)bmpPtr;

        //        //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
        //        Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {

        //    }

        //    return byteArray;
        //}



        //public static unsafe Bitmap ConcatenateBitmapParts(
        //    System.Drawing.Imaging.BitmapData[] bmpDataArray,
        //    int width,
        //    int height,
        //    PixelFormat pixelFormat)
        //{

        //    Bitmap bmpOut = new Bitmap(width, height, pixelFormat);

        //    Rectangle outRect = new Rectangle(0, 0, width, height);
        //    BitmapData bmpData = bmpOut.LockBits(outRect, ImageLockMode.ReadWrite, pixelFormat);

        //    if (bmpData == null) return null;

        //    int channels = getNumOfChannels(bmpData.PixelFormat);
        //    int byteDepth = getByteDepth(bmpData.PixelFormat);
        //    int pixel = channels * byteDepth;

        //    fromPixel = (fromPixel * pixel);
        //    int numOfBytes = (numberOfPixel * pixel);

        //    // create the byte array of specific length
        //    byte[] byteArray = new byte[numOfBytes];

        //    try
        //    {
        //        byte* bmpPtr = (byte*)bmpData.Scan0;
        //        bmpPtr += fromPixel;
        //        IntPtr getScan0Ptr = (IntPtr)bmpPtr;

        //        //Buffer.BlockCopy(
        //        //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
        //        Marshal.Copy(getScan0Ptr, byteArray, 0, numOfBytes);
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {

        //    }

        //    return byteArray;
        //}



        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed short (Int16) array
        /// </summary>
        /// <param name="bmp">Input <see cref="System.Drawing.Bitmap"/>.</param>
        /// <returns>Int16[]: short(Int16) array</returns>
        public static Int16[] CopyBitmapToInt16Array(System.Drawing.Bitmap bmp)
        {

            if (bmp.PixelFormat == PixelFormat.Format32bppArgb || bmp.PixelFormat == PixelFormat.Format64bppArgb) 
                return null;

            int channels = getNumOfChannels(bmp.PixelFormat);
            int depth = getByteDepth(bmp.PixelFormat);
            //int byteDepth = getByteDepth(bmp.PixelFormat);
            int numOfBytes = (bmp.Width * bmp.Height) * channels * depth;
            int numOfWords = (bmp.Width * bmp.Height) * channels;

            // create the byte array of specific length
            Int16[] byteArray = new Int16[numOfWords];
            BitmapData bmpData = null;

            try
            {

                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);



                IntPtr getScan0Ptr = bmpData.Scan0;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfWords);

            }
            catch
            {
                return null;
            }
            finally
            {

                //Unlock the pixels
                bmp.UnlockBits(bmpData);
                //if (bmp != null)
                //    bmp.Dispose();

            }

            //Return the bytearray 
            return byteArray;
        }



        /// <summary>
        /// (This works 100% - instead of using memorystreams use this method !!!)
        /// Copies unmanaged BitmapData into a managed int (Int32) array.
        /// </summary>
        /// <param name="bmp">Input <see cref="System.Drawing.Bitmap"/>.</param>
        /// <returns>Int32[]: int(Int32) array</returns>
        public static Int32[] CopyBitmapToInt32Array(System.Drawing.Bitmap bmp)
        {

            if (bmp.PixelFormat == PixelFormat.Format16bppGrayScale
                || bmp.PixelFormat == PixelFormat.Format64bppArgb) return null;

            int channels = getNumOfChannels(bmp.PixelFormat);
            int depth = getByteDepth(bmp.PixelFormat);
            //int byteDepth = getByteDepth(bmp.PixelFormat);
            int numOfBytes = (bmp.Width * bmp.Height) * channels * depth;
            int numOfWords = (bmp.Width * bmp.Height) * 2;
            int numOfDWords = (bmp.Width * bmp.Height) * depth;


            // create the byte array of specific length
            Int32[] byteArray = new Int32[numOfDWords];
            BitmapData bmpData = null;

            try
            {

                //Create a BitmapData and Lock all pixels to be written 
                bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);



                IntPtr getScan0Ptr = bmpData.Scan0;

                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                Marshal.Copy(getScan0Ptr, byteArray, 0, numOfDWords);

            }
            catch
            {
                return null;
            }
            finally
            {

                //Unlock the pixels
                bmp.UnlockBits(bmpData);
                //if (bmp != null)
                //    bmp.Dispose();

            }

            //Return the byteArray 
            return byteArray;
        }




        /// <summary>
        /// Creates a perfect 1:1 copy of a <see cref="System.Drawing.Bitmap"/> (fastest possible way!).
        /// </summary>
        /// <param name="Bmp">Bitmap to be copied.</param>
        /// <returns>Copied bitmap.</returns>
        public static Bitmap GetExactBitmapCopy(System.Drawing.Bitmap Bmp)
        {

            System.Drawing.Bitmap newImage = null;

            byte[] byteArray = null;


            try
            {

                byteArray =
                    IPVSBaseOpsClasses.CImageUtility.CopyBitmapToByteArray(Bmp);

                newImage =
                    IPVSBaseOpsClasses.CImageUtility.CopyByteArrayToBitmap(Bmp.Width, Bmp.Height, Bmp.PixelFormat, byteArray);

            }
            finally
            {

                //Bmp.Dispose();
                byteArray = null;
                GC.Collect();
            }

            return newImage;
        }




        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="inBmp"></param>
        ///// <param name="width"></param>
        ///// <param name="height"></param>
        ///// <param name="toPxlFormat"></param>
        ///// <param name="convEnum"></param>
        ///// <returns></returns>
        //public static unsafe Bitmap convertBitmapPxlFormats(
        //    System.Drawing.Bitmap inBmp, 
        //    int width, 
        //    int height, 
        //    PixelFormat toPxlFormat,
        //    GreyScaleConversionEnum convEnum)
        //{


        //    System.Drawing.Bitmap outBmp = null;


        //    // Check if toPxlFormat is 16bppGrayscale
        //    if (toPxlFormat != PixelFormat.Format16bppGrayScale
        //        && toPxlFormat != PixelFormat.Format48bppRgb
        //        && toPxlFormat != PixelFormat.Format64bppArgb)
        //    {

        //        FreeImageAPI.FREE_IMAGE_COLOR_DEPTH ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_UNKNOWN;


        //        switch (toPxlFormat)
        //        {
        //            case PixelFormat.Format1bppIndexed:
        //                {
        //                    ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_01_BPP;
        //                    break;
        //                }
        //            case PixelFormat.Format4bppIndexed:
        //                {
        //                    ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_04_BPP;
        //                    break;
        //                }
        //            case PixelFormat.Format8bppIndexed:
        //                {
        //                    if (convEnum == GreyScaleConversionEnum.none)
        //                        ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_08_BPP;
        //                    else
        //                        ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_FORCE_GREYSCALE;
        //                    break;
        //                }
        //            case PixelFormat.Format24bppRgb:
        //                {
        //                    ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_24_BPP;
        //                    break;
        //                }
        //            case PixelFormat.Format32bppArgb:
        //                {
        //                    ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP;
        //                    break;
        //                }
        //            default:
        //                {
        //                    ficd = FreeImageAPI.FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP;
        //                    break;
        //                }
        //        }



        //        // use FreeImage Libray to change the pixelformat

        //        FreeImageAPI.FreeImageBitmap fib = new FreeImageAPI.FreeImageBitmap(inBmp);

        //        fib.ConvertColorDepth(ficd);

        //        outBmp = fib.ToBitmap();


        //    }
        //    else if (toPxlFormat == PixelFormat.Format16bppGrayScale
        //        && toPxlFormat == PixelFormat.Format48bppRgb
        //        && toPxlFormat == PixelFormat.Format64bppArgb)
        //    {

        //        //// Bitmap output
        //        outBmp = new Bitmap(width, height, toPxlFormat);




        //    }
        //    else
        //        return inBmp;

            

        //    // Create a array of double values


        //    ////// TODO: INSERT AND CORRECT CODE 
        //    //System.Drawing.Bitmap outBmp = null;
        //    //// Bitmap output
        //    //outBmp = new Bitmap(width, height, toPxlFormat);



        //    //System.Drawing.Rectangle outRect = new System.Drawing.Rectangle(0, 0, width, height);

        //    //System.Drawing.Imaging.BitmapData bmpDataout = outBmp.LockBits(outRect, ImageLockMode.ReadWrite, outBmp.PixelFormat);
        //    ////System.Drawing.Imaging.BitmapData tempBMPData = tempBMP.LockBits(tempBMPRect, ImageLockMode.ReadWrite, tempBMP.PixelFormat);

        //    //int InChannels = getNumOfChannels(fromPxlFormat), OutChannels = getNumOfChannels(toPxlFormat);
        //    //int x = 0, y = 0, ptrCounter = 0, lineoffs = 0;


        //    //int* ptrInChannels = &InChannels, ptrOutChannels = &OutChannels;

        //    //int totalNumberPixel = (width * height);
        //    //int* ptrTotalNPx = &totalNumberPixel;
        //    //int* ptrHeight = &height, ptrWidth = &width;


        //    //#region (OutChannels == 4 && toPxlFormat == PixelFormat.Format32bppArgb)

        //    //if (OutChannels == 4 && toPxlFormat == PixelFormat.Format32bppArgb)
        //    //{
        //    //    fixed (byte* IPlptr = inputByteArray)
        //    //        try
        //    //        {

        //    //            //byte* ptrOut = (byte*)(bmpDataout.Scan0);
        //    //            //byte* ptrIPlData = IPlptr;

        //    //            switch (*ptrInChannels)
        //    //            {
        //    //                case 1:
        //    //                    {

                                

        //    //                        break;
        //    //                    }
        //    //                case 3:
        //    //                    {



        //    //                        if (fromPxlFormat == PixelFormat.Format24bppRgb)
        //    //                        {


        //    //                        }

        //    //                        if (fromPxlFormat == PixelFormat.Format48bppRgb)
        //    //                        {


        //    //                            byte* ptrIPlData = IPlptr;

        //    //                            byte[] LUT = generate16BitTo8BitLUT();

        //    //                            byte* ptr = (byte*)bmpDataout.Scan0;
        //    //                            byte tempVal = 0;
        //    //                            byte* ptrTempVal = &tempVal;
        //    //                            int index = 0;
        //    //                            int* ptrIndex = &index;

        //    //                            lineoffs = bmpDataout.Stride - (width * 3 * 2); // !!!!  * 2 because its 16 bit per color channel

        //    //                            fixed (byte* ptrLUT = LUT)
        //    //                            for (y = 0; y < (*ptrHeight); y++)
        //    //                            {
        //    //                                for (x = 0; x < (*ptrWidth); x++)
        //    //                                {


        //    //                                    *ptrIndex = ptrIPlData[ptrCounter + 0] & (ptrIPlData[ptrCounter + 1] << 8);
        //    //                                    *ptrTempVal = ptrLUT[*ptrIndex];

        //    //                                    //blue
        //    //                                    *ptr = *ptrTempVal;
        //    //                                    ptr++;


        //    //                                    *ptrIndex = ptrIPlData[ptrCounter + 2] & (ptrIPlData[ptrCounter + 3] << 8);
        //    //                                    *ptrTempVal = ptrLUT[*ptrIndex];

        //    //                                    // green 
        //    //                                    *ptr = *ptrTempVal;
        //    //                                    ptr++;


        //    //                                    *ptrIndex = ptrIPlData[ptrCounter + 4] & (ptrIPlData[ptrCounter + 5] << 8);
        //    //                                    *ptrTempVal = ptrLUT[*ptrIndex];

        //    //                                    // red
        //    //                                    *ptr = *ptrTempVal;
        //    //                                    ptr++;


        //    //                                    // alpha
        //    //                                    *ptr = 255;
        //    //                                    ptr++;


        //    //                                    ptrCounter += 6;

        //    //                                }

        //    //                                ptrCounter += lineoffs*2;
        //    //                            }




        //    //                        }




        //    //                        break;
        //    //                    }
        //    //                case 4:
        //    //                    {


                                   

        //    //                        break;
        //    //                    }
        //    //                default:
        //    //                    {


                                   
        //    //                        break;
        //    //                    }
        //    //            }
        //    //        }
        //    //        finally
        //    //        {
        //    //            outBmp.UnlockBits(bmpDataout);
        //    //        }
        //    //}

        //    //#endregion



        //    //#region (OutChannels == 1 && toPxlFormat == PixelFormat.Format16bppGrayScale)

        //    //if (OutChannels == 1 && toPxlFormat == PixelFormat.Format16bppGrayScale)
        //    //{

        //    //    fixed (byte* IPlptr = inImg)
        //    //        try
        //    //        {


        //    //            // TODO: Right Code to enter !!

        //    //            byte* ptrOut = (byte*)(bmpDataout.Scan0);
        //    //            byte* ptrIPlData = IPlptr;

        //    //            switch (*ptrInChannels)
        //    //            {
        //    //                case 1:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (*ptrIPlData);


        //    //                                ptrOut += 1;
        //    //                                ptrIPlData += *ptrInChannels;
        //    //                            }
        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                case 3:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (ptrIPlData[0]);
        //    //                                ptrOut[1] = (ptrIPlData[1]);
        //    //                                ptrOut[2] = (ptrIPlData[2]);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += *ptrInChannels;
        //    //                            }
        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                case 4:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (ptrIPlData[0]);
        //    //                                ptrOut[1] = (ptrIPlData[1]);
        //    //                                ptrOut[2] = (ptrIPlData[2]);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += *ptrInChannels;
        //    //                            }
        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                default:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (*ptrIPlData);
        //    //                                ptrOut[1] = (*ptrIPlData);
        //    //                                ptrOut[2] = (*ptrIPlData);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += 1;
        //    //                            }
        //    //                        }
        //    //                        break;
        //    //                    }
        //    //            }




        //    //        }
        //    //        finally
        //    //        {
        //    //            outBmp.UnlockBits(bmpDataout);
        //    //        }
        //    //}


        //    //#endregion



        //    //#region (OutChannels == 4 && toPxlFormat == PixelFormat.Format64bppArgb)

        //    //if (OutChannels == 4 && toPxlFormat == PixelFormat.Format32bppArgb)
        //    //{

        //    //    fixed (byte* IPlptr = inImg)
        //    //        try
        //    //        {

        //    //            byte* ptrOut = (byte*)(bmpDataout.Scan0);
        //    //            byte* ptrIPlData = IPlptr;

        //    //            switch (*ptrInChannels)
        //    //            {
        //    //                case 1:
        //    //                    {

        //    //                        if (fromPxlFormat == PixelFormat.Format16bppGrayScale)
        //    //                        {

        //    //                            // TODO: INSERT AND CORRECT CODE 

        //    //                            for (int j = 0; j < height; j++)
        //    //                            {
        //    //                                for (int i = 0; i < width; i++)
        //    //                                {
        //    //                                    ptrOut[0] = (*ptrIPlData);
        //    //                                    ptrOut[1] = (*ptrIPlData);
        //    //                                    ptrOut[2] = (*ptrIPlData);
        //    //                                    ptrOut[3] = 255;

        //    //                                    ptrOut += 4;
        //    //                                    ptrIPlData += *ptrInChannels;
        //    //                                }
        //    //                            }

        //    //                        }
        //    //                        else
        //    //                        {

        //    //                            for (int j = 0; j < height; j++)
        //    //                            {
        //    //                                for (int i = 0; i < width; i++)
        //    //                                {
        //    //                                    ptrOut[0] = (*ptrIPlData);
        //    //                                    ptrOut[1] = (*ptrIPlData);
        //    //                                    ptrOut[2] = (*ptrIPlData);
        //    //                                    ptrOut[3] = 255;

        //    //                                    ptrOut += 4;
        //    //                                    ptrIPlData += *ptrInChannels;
        //    //                                }
        //    //                            }

        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                case 3:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (ptrIPlData[0]);
        //    //                                ptrOut[1] = (ptrIPlData[1]);
        //    //                                ptrOut[2] = (ptrIPlData[2]);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += *ptrInChannels;
        //    //                            }
        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                case 4:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (ptrIPlData[0]);
        //    //                                ptrOut[1] = (ptrIPlData[1]);
        //    //                                ptrOut[2] = (ptrIPlData[2]);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += *ptrInChannels;
        //    //                            }
        //    //                        }

        //    //                        break;
        //    //                    }
        //    //                default:
        //    //                    {


        //    //                        for (int j = 0; j < height; j++)
        //    //                        {
        //    //                            for (int i = 0; i < width; i++)
        //    //                            {
        //    //                                ptrOut[0] = (*ptrIPlData);
        //    //                                ptrOut[1] = (*ptrIPlData);
        //    //                                ptrOut[2] = (*ptrIPlData);
        //    //                                ptrOut[3] = 255;

        //    //                                ptrOut += 4;
        //    //                                ptrIPlData += 1;
        //    //                            }
        //    //                        }
        //    //                        break;
        //    //                    }
        //    //            }
        //    //        }
        //    //        finally
        //    //        {
        //    //            outBmp.UnlockBits(bmpDataout);
        //    //        }
        //    //}

        //    //#endregion



        //    return outBmp;
        //}




        /// <summary>
        /// Checks the input pixelformat and returns an IPV# compatible <see cref="System.Drawing.Imaging.PixelFormat"/>.
        /// </summary>
        /// <param name="inPxlFormat">The input  <see cref="System.Drawing.Imaging.PixelFormat"/>.</param>
        /// <returns>The compatible <see cref="System.Drawing.Imaging.PixelFormat"/>.</returns>
        public static System.Drawing.Imaging.PixelFormat getCompatibleIPVPixelFormat(System.Drawing.Imaging.PixelFormat inPxlFormat)
        {

            System.Drawing.Imaging.PixelFormat tempPxlFormat = System.Drawing.Imaging.PixelFormat.Undefined;

            switch (inPxlFormat)
            {
                //
                // Format16bppGrayScale
                //
                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    {
                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format16bppGrayScale;
                        break;
                    }
                //
                // Format32bppArgb
                //
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb555:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
                    {

                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
                //
                // Format64bppArgb
                //
                case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
                    {
                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format64bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
                    {
                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format64bppArgb;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
                    {
                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format64bppArgb;
                        break;
                    }
                default:
                    {
                        tempPxlFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        break;
                    }
            }
            return tempPxlFormat;
        }



        /// <summary>
        /// Loads a bitmap from Disc - the format of the bitmap remains unchanged.
        /// Most importent method for loading images. Do not use .Net mthods instead.
        /// </summary>
        /// <param name="imagePath">The image path.</param>
        /// <returns>The successfuly loaded <see cref="System.Drawing.Bitmap"/>.</returns>
        public static Bitmap loadImage(System.String imagePath)
        {


            //this.MethodID = "openImages";

            // Initialization part
            // the FIBITMAP (FreeImage .Net)
            FreeImageAPI.FIBITMAP dib = FreeImageAPI.FIBITMAP.Zero;
            // The reference to the bitmap to be loaded 
            System.Drawing.Bitmap original = null;

            //System.Drawing.Imaging.PixelFormat tempPxlFormat = System.Drawing.Imaging.PixelFormat.Undefined;

            // FreeImage .Net enums for loading images correctly
            FreeImageAPI.FREE_IMAGE_FORMAT fif = FreeImageAPI.FREE_IMAGE_FORMAT.FIF_UNKNOWN;
            FreeImageAPI.FREE_IMAGE_LOAD_FLAGS fil = FreeImageAPI.FREE_IMAGE_LOAD_FLAGS.DEFAULT;

            FreeImageAPI.FreeImageBitmap fib = null;


            try
            {


                // get the filetype from file name extension !
                fif = FreeImageAPI.FreeImage.GetFIFFromFilename(imagePath);


                // if filetype is a known filetype do ->
                if (fif != FreeImageAPI.FREE_IMAGE_FORMAT.FIF_UNKNOWN)
                {


                    #region 1.switch block
                    switch (fif)
                    {

                        // if it's a jpeg file, load it correctly with best quality
                        case FreeImageAPI.FREE_IMAGE_FORMAT.FIF_JPEG:
                            {

                                // set the right loading flag 
                                fil = FreeImageAPI.FREE_IMAGE_LOAD_FLAGS.JPEG_ACCURATE;
                                // create a new freeImage bitmap from file on disc
                                fib = new FreeImageAPI.FreeImageBitmap(imagePath, fif, fil);
                                // convert the FreeImageBitmap to a .Net Bitmap and set it to the reference 'original'
                                original = fib.ToBitmap();

                                if (fib != null)
                                    fib.Dispose();

                                break;
                            }
                        // when the image is a tiff file, some problems may occure - > 16, 48 and 64 Bit bitmaps cannot be transformed from 
                        // FreeImageBitmap to .Net bitmap -> therefore new way is necessary for loading that kinds of bitmaps
                        case FreeImageAPI.FREE_IMAGE_FORMAT.FIF_TIFF:
                            {


                                dib = FreeImageAPI.FreeImage.Load(fif, imagePath, fil);

                                int pixelDepth = (int)FreeImageAPI.FreeImage.GetBPP(dib);


                                #region 2. switch block
                                switch (pixelDepth)
                                {
                                    case 16:
                                        {

                                            int width = (int)FreeImageAPI.FreeImage.GetWidth(dib);
                                            int height = (int)FreeImageAPI.FreeImage.GetHeight(dib);
                                            int scan_width = (int)FreeImageAPI.FreeImage.GetPitch(dib);

                                            int numOfBytes = height * System.Math.Abs(scan_width);

                                            byte[] tempByteArray = new byte[numOfBytes];

                                            //FreeImageAPI.FIBITMAP dibRAWBits = FreeImageAPI.FreeImage.ConvertToRawBits(dib);


                                            FreeImageAPI.FreeImage.ConvertToRawBits(
                                                tempByteArray,
                                                dib,
                                                scan_width,
                                                (uint)pixelDepth,
                                                FreeImageAPI.FreeImage.FI_RGBA_RED_MASK,
                                                FreeImageAPI.FreeImage.FI_RGBA_GREEN_MASK,
                                                FreeImageAPI.FreeImage.FI_RGBA_BLUE_MASK, true);


                                            //int width = (int)FreeImageAPI.FreeImage.GetWidth(dibRAWBits);
                                            //int height = (int)FreeImageAPI.FreeImage.GetHeight(dibRAWBits);
                                            //int scan_width = (int)FreeImageAPI.FreeImage.GetPitch(dibRAWBits);



                                            //IntPtr getScan0Ptr = FreeImageAPI.FreeImage. dib32Bits;

                                            original = new Bitmap(width, height, PixelFormat.Format16bppGrayScale);


                                            BitmapData bmpData = null;


                                            try
                                            {



                                                //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                                                //Marshal.Copy(getScan0Ptr, tempByteArray, 0, numOfBytes);


                                                // Lock all pixels to be written 
                                                bmpData = original.LockBits(
                                                    new Rectangle(0, 0, original.Width, original.Height),
                                                    ImageLockMode.ReadWrite, original.PixelFormat);

                                                //Copy the data from the byte array into BitmapData.Scan0
                                                Marshal.Copy(tempByteArray, 0, bmpData.Scan0, tempByteArray.Length);


                                            }
                                            finally
                                            {

                                                //Unlock the pixels
                                                if (original != null) ;
                                                    original.UnlockBits(bmpData);
                                                if (!dib.IsNull)
                                                    FreeImageAPI.FreeImage.Unload(dib);
                                                tempByteArray = null;
                                                GC.Collect();

                                            }

                                            break;
                                        }
                                    case 48:
                                        {


                                            //int width = (int)FreeImageAPI.FreeImage.GetWidth(dib);
                                            //int height = (int)FreeImageAPI.FreeImage.GetHeight(dib);
                                            //int scan_width = (int)FreeImageAPI.FreeImage.GetPitch(dib);

                                            //int numOfBytes = height * System.Math.Abs(scan_width);

                                            //byte[] tempByteArray = new byte[numOfBytes];

                                            ////FreeImageAPI.FIBITMAP dibRAWBits = FreeImageAPI.FreeImage.ConvertToRawBits(dib);

                                            //FreeImageAPI.FreeImage.ConvertToRawBits(
                                            //    tempByteArray,
                                            //    dib,
                                            //    scan_width,
                                            //    (uint)pixelDepth,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_RED_MASK,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_GREEN_MASK,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_BLUE_MASK, true);



                                            //original = new Bitmap(width, height, PixelFormat.Format48bppRgb);


                                            //BitmapData bmpData = null;


                                            //try
                                            //{



                                            //    //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                                            //    //Marshal.Copy(getScan0Ptr, tempByteArray, 0, numOfBytes);


                                            //    // Lock all pixels to be written 
                                            //    bmpData = original.LockBits(
                                            //        new Rectangle(0, 0, original.Width, original.Height),
                                            //        ImageLockMode.ReadWrite, original.PixelFormat);

                                            //    //Copy the data from the byte array into BitmapData.Scan0
                                            //    Marshal.Copy(tempByteArray, 0, bmpData.Scan0, tempByteArray.Length);


                                            //}
                                            //finally
                                            //{

                                            //    //Unlock the pixels
                                            //    if (original != null) ;
                                            //    original.UnlockBits(bmpData);
                                            //    if (!dib.IsNull)
                                            //        FreeImageAPI.FreeImage.Unload(dib);
                                            //    tempByteArray = null;
                                            //    GC.Collect();

                                            //}

                                            original = new System.Drawing.Bitmap(imagePath);

                                            if (!dib.IsNull)
                                                FreeImage.Unload(dib);

                                            break;
                                        }
                                    case 64:
                                        {

                                            //int width = (int)FreeImageAPI.FreeImage.GetWidth(dib);
                                            //int height = (int)FreeImageAPI.FreeImage.GetHeight(dib);
                                            //int scan_width = (int)FreeImageAPI.FreeImage.GetPitch(dib);

                                            //int numOfBytes = height * System.Math.Abs(scan_width);

                                            //byte[] tempByteArray = new byte[numOfBytes];

                                            ////FreeImageAPI.FIBITMAP dibRAWBits = FreeImageAPI.FreeImage.ConvertToRawBits(dib);

                                            //FreeImageAPI.FreeImage.ConvertToRawBits(
                                            //    tempByteArray,
                                            //    dib,
                                            //    scan_width,
                                            //    (uint)pixelDepth,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_RED_MASK,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_GREEN_MASK,
                                            //    FreeImageAPI.FreeImage.FI_RGBA_BLUE_MASK, true);


                                            //original = new Bitmap(width, height, PixelFormat.Format64bppArgb);


                                            //BitmapData bmpData = null;


                                            //try
                                            //{



                                            //    //Copy the bitmap from pointer BitmapData.Scan0 into the byte array 
                                            //    //Marshal.Copy(getScan0Ptr, tempByteArray, 0, numOfBytes);


                                            //    // Lock all pixels to be written 
                                            //    bmpData = original.LockBits(
                                            //        new Rectangle(0, 0, original.Width, original.Height),
                                            //        ImageLockMode.ReadWrite, original.PixelFormat);

                                            //    //Copy the data from the byte array into BitmapData.Scan0
                                            //    Marshal.Copy(tempByteArray, 0, bmpData.Scan0, tempByteArray.Length);


                                            //}
                                            //finally
                                            //{

                                            //    //Unlock the pixels
                                            //    if (original != null)
                                            //        original.UnlockBits(bmpData);

                                            //    if (!dib.IsNull)
                                            //        FreeImageAPI.FreeImage.Unload(dib);

                                            //    tempByteArray = null;
                                            //    GC.Collect();

                                            //}

                                            original = new System.Drawing.Bitmap(imagePath);

                                            if (!dib.IsNull)
                                                FreeImage.Unload(dib);

                                            break;
                                        }
                                    default:
                                        {

                                            fib = new FreeImageAPI.FreeImageBitmap(imagePath, fif, fil);
                                            original = fib.ToBitmap();

                                            if (fib != null)
                                                fib.Dispose();

                                            if (!dib.IsNull)
                                                FreeImage.Unload(dib);
                                            break;
                                        }
                                }

                                #endregion 2. switch block



                                break;
                            }
                        default:
                            {

                                fib = new FreeImageAPI.FreeImageBitmap(imagePath, fif, fil);
                                original = fib.ToBitmap();

                                if (fib != null)
                                    fib.Dispose();

                                break;
                            }
                    }



                    #endregion 1. switch block


                    if (original == null)
                        original = new System.Drawing.Bitmap(imagePath);
                }
                else
                {

                    if (original == null)
                        original = new System.Drawing.Bitmap(imagePath);
                }


            }
            catch (Exception ex)
            {

                //this.OnInvoking(new OnInvokingEventArgs(
                //    ConsoleMsgEnum.Debug, ex.Message, OperationStatusEnum.Error));
                return original;

            }
            finally
            {



            }


            return original;
        }




        /// <summary>
        /// Converts a 16 bpp grayscale <see cref="System.Drawing.Bitmap" /> instance with 
        /// <see cref="System.Drawing.Imaging.PixelFormat.Format16bppGrayScale" /> into a 
        /// <see cref="System.Drawing.Imaging.PixelFormat.Format32bppArgb" /> grayscale 
        /// <see cref="System.Drawing.Bitmap" /> instance.
        /// </summary>
        /// <param name="inBmp">The input bitmap.</param>
        /// <returns>
        /// The converted <see cref="System.Drawing.Bitmap" /> instance.
        /// </returns>
        public unsafe static Bitmap convert16bppGrayTo32bppARGB(Bitmap inBmp)
        {
            System.Drawing.Bitmap returnBmp = null;
            BitmapData bmpData = null;
            short[] shortArray = null;
            byte[] LUT = null;

            if (inBmp.PixelFormat != PixelFormat.Format16bppGrayScale) return inBmp;

            try
            {

                shortArray = CopyBitmapToInt16Array(inBmp);
                LUT = generate16BitTo8BitLUT();

                fixed (short* ptrShortArray = shortArray)
                fixed (byte* ptrLUT = LUT)
                {
                    short* ptrShortA = ptrShortArray;
                    for (int i = 0; i < shortArray.Length; i++)
                    {
                        ushort temp = (ushort)ptrShortA[i];
                        ptrShortA[i] = (short)ptrLUT[temp];
                    }
                }


                returnBmp = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);


                bmpData = returnBmp.LockBits(
                    new Rectangle(0, 0, returnBmp.Width, returnBmp.Height),
                    ImageLockMode.ReadWrite, returnBmp.PixelFormat);

                byte* ptrScan0 = (byte*)bmpData.Scan0;


                fixed (short* ptrShortArray = shortArray)
                {
                    short* ptrShortA = ptrShortArray;
                    for (int i = 0; i < shortArray.Length; i++)
                    {
                        ptrScan0[0] = (byte)ptrShortA[i];
                        ptrScan0[1] = (byte)ptrShortA[i];
                        ptrScan0[2] = (byte)ptrShortA[i];
                        ptrScan0[3] = 255;

                        ptrScan0 += 4;
                    }
                }

            }
            finally
            {
                returnBmp.UnlockBits(bmpData);
                shortArray = null;
                LUT = null;
                GC.Collect();
            }

            return returnBmp;
        }



        /// <summary>
        /// Converts any bitmap with arbitrary <see cref="System.Drawing.Imaging.PixelFormat" /> to
        /// the desired <see cref="System.Drawing.Imaging.PixelFormat.Format32bppArgb" /> pixelformat.
        /// </summary>
        /// <param name="inBmp">Input bitmap.</param>
        /// <returns>
        /// The converted <see cref="System.Drawing.Bitmap" /> instance.
        /// </returns>
        public unsafe static Bitmap tryConvertBitmapTo32ARGB(System.Drawing.Bitmap inBmp)
        {

            Bitmap returnBitmap = null;
            if (inBmp == null) return null;

            switch (inBmp.PixelFormat)
            {
                case PixelFormat.Format24bppRgb:
                    {

                        try
                        {

                            returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);

                            using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                            {

                                g.DrawImage(
                                    inBmp,
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    GraphicsUnit.Pixel);

                            }

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {

                        }

                        break;
                    }
                case PixelFormat.Format1bppIndexed:
                    {


                        FreeImageBitmap fib = null;

                        try
                        {
                            fib = new FreeImageBitmap(inBmp);

                            fib.ConvertColorDepth(FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP);

                            returnBitmap = fib.ToBitmap();

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {
                            if (fib != null)
                                fib.Dispose();
                        }

                        break;
                    }
                case PixelFormat.Format8bppIndexed:
                    {
                        FreeImageBitmap fib = null;

                        try
                        {
                            fib = new FreeImageBitmap(inBmp);

                            fib.ConvertColorDepth(FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP);

                            returnBitmap = fib.ToBitmap();

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {
                            if (fib != null)
                                fib.Dispose();
                        }

                        break;
                    }
                case PixelFormat.Format4bppIndexed:
                    {
                        FreeImageBitmap fib = null;

                        try
                        {
                            fib = new FreeImageBitmap(inBmp);

                            fib.ConvertColorDepth(FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP);

                            returnBitmap = fib.ToBitmap();

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {
                            if (fib != null)
                                fib.Dispose();
                        }

                        break;
                    }
                case PixelFormat.Format48bppRgb:
                    {
                        try
                        {

                            returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);

                            using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                            {

                                g.DrawImage(
                                    inBmp,
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    GraphicsUnit.Pixel);

                            }

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {

                        }

                        break;
                    }
                case PixelFormat.Format64bppArgb:
                    {
                        try
                        {

                            returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);

                            using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                            {

                                g.DrawImage(
                                    inBmp,
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    GraphicsUnit.Pixel);

                            }

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {

                        }

                        break;
                    }
                case PixelFormat.Format16bppGrayScale:
                    {


                        try
                        {


                            returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);


                            inBmp = convert16bppGrayTo32bppARGB(inBmp);

                            // Format16bppGrayScale cannot be drawn by gdi !!!  
                            // so, it has to be converted to be displayable

                            using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                            {

                                g.DrawImage(
                                    inBmp,
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    GraphicsUnit.Pixel);

                            }

                        }
                        catch
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {

                        }


                        break;
                    }
                case PixelFormat.Format32bppArgb:
                    {

                        returnBitmap = GetExactBitmapCopy(inBmp);

                        break;
                    }
                default:
                    {
                        try
                        {

                            returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);

                            using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                            {

                                g.DrawImage(
                                    inBmp,
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                    GraphicsUnit.Pixel);

                            }

                        }
                        catch
                        {
                            returnBitmap = GetExactBitmapCopy(inBmp);
                        }
                        finally
                        {

                        }

                        break;

                    }

            }

            return returnBitmap;
        }



        /// <summary>
        /// Converts an any arbitrary input <see cref="System.Drawing.Bitmap" /> instance to an IPV# 
        /// compatible <see cref="System.Drawing.Bitmap" /> instance.
        /// </summary>
        /// <param name="inBmp">The input Bitmap to be converted.</param>
        /// <returns>
        /// An IPV# compatible image processing <see cref="System.Drawing.Bitmap"/> instance.
        /// </returns>
        public static System.Drawing.Bitmap getIPVCompatibleProcessingBitmap(System.Drawing.Bitmap inBmp)
        {

            System.Drawing.Bitmap returnBitmap = null;

            System.Drawing.Imaging.PixelFormat tempPxlFormat = System.Drawing.Imaging.PixelFormat.Undefined;

            if (inBmp == null) return null;

            try
            {

                tempPxlFormat = getCompatibleIPVPixelFormat(inBmp.PixelFormat);


                switch (inBmp.PixelFormat)
                {
                    case PixelFormat.Format24bppRgb:
                        {

                            returnBitmap = tryConvertBitmapTo32ARGB(inBmp);

                            break;
                        }
                    case PixelFormat.Format1bppIndexed:
                        {

                            returnBitmap = tryConvertBitmapTo32ARGB(inBmp);

                            break;
                        }
                    case PixelFormat.Format8bppIndexed:
                        {
                            returnBitmap = tryConvertBitmapTo32ARGB(inBmp);

                            break;
                        }
                    case PixelFormat.Format4bppIndexed:
                        {
                            returnBitmap = tryConvertBitmapTo32ARGB(inBmp);

                            break;
                        }
                    case PixelFormat.Format48bppRgb:
                        {

                            try
                            {

                                returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, tempPxlFormat);

                                using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                                {

                                    g.DrawImage(
                                        inBmp,
                                        new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                        new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                        GraphicsUnit.Pixel);

                                }

                            }
                            catch
                            {

                                return inBmp;
                            }
                            finally
                            {

                            }

                            break;
                        }
                    case PixelFormat.Format64bppArgb:
                        {
                            returnBitmap = GetExactBitmapCopy(inBmp);

                            break;
                        }
                    case PixelFormat.Format32bppArgb:
                        {
                            returnBitmap = GetExactBitmapCopy(inBmp);

                            break;
                        }
                    case PixelFormat.Format16bppGrayScale:
                        {

                            returnBitmap = GetExactBitmapCopy(inBmp);

                            break;
                        }
                    default:
                        {

                            try
                            {

                                returnBitmap = new Bitmap(inBmp.Width, inBmp.Height, PixelFormat.Format32bppArgb);

                                using (System.Drawing.Graphics g = Graphics.FromImage(returnBitmap))
                                {

                                    g.DrawImage(
                                        inBmp,
                                        new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                        new Rectangle(0, 0, inBmp.Width, inBmp.Height),
                                        GraphicsUnit.Pixel);

                                }

                            }
                            catch
                            {

                                returnBitmap = GetExactBitmapCopy(inBmp);
                            }
                            finally
                            {

                            }

                            break;

                        }
                }

            }
            finally
            {

                if (inBmp != null)
                    inBmp.Dispose();
            }

            return returnBitmap;
        }



        /// <summary>
        /// Returns a Format32bppARgb/Format16bppGrayScale/Format64bppArgb Bitmap object related to the
        /// original Bitmap source at <paramref name="FilePath"/>.
        /// </summary>
        /// <param name="FilePath">The file path to the Bitmap.</param>
        /// <returns>
        /// An IPV# compatible image processing <see cref="System.Drawing.Bitmap"/> instance.
        /// </returns>
        private static System.Drawing.Bitmap getIPVCompatibleProcessingBitmap(System.String FilePath)
        {


            System.Drawing.Bitmap original = null;
            //System.Drawing.Imaging.PixelFormat tempPxlFormat = System.Drawing.Imaging.PixelFormat.Undefined;


            System.Drawing.Bitmap IpImg = null;

            try
            {

                original = loadImage(FilePath);


                IpImg = IPVSBaseOpsClasses.CImageUtility.getIPVCompatibleProcessingBitmap(original);

            }
            finally
            {
                if (original != null)
                    original.Dispose();

            }


            return IpImg;
        }


        /// <summary>
        /// Gets the pixel format.
        /// </summary>
        /// <param name="bitDepth">The bit depth.</param>
        /// <param name="noChannels">The no. channels.</param>
        /// <returns><see cref="System.Drawing.Imaging.PixelFormat"/></returns>
        public static System.Drawing.Imaging.PixelFormat getPixelFormat(int bitDepth, int noChannels)
        {

            switch (noChannels)
            {
                case 1:
                    {
                        if (bitDepth == 8)
                            return System.Drawing.Imaging.PixelFormat.Format8bppIndexed;
                        else if (bitDepth == 16)
                            return System.Drawing.Imaging.PixelFormat.Format16bppGrayScale;
                        else return System.Drawing.Imaging.PixelFormat.Undefined;
                    }
                case 3:
                    {
                        if (bitDepth == 24)
                            return System.Drawing.Imaging.PixelFormat.Format24bppRgb;
                        else if (bitDepth == 48)
                            return System.Drawing.Imaging.PixelFormat.Format48bppRgb;
                        else return System.Drawing.Imaging.PixelFormat.Undefined;
                    }
                case 4:
                    {
                        if (bitDepth == 32)
                            return System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                        else if (bitDepth == 64)
                            return System.Drawing.Imaging.PixelFormat.Format64bppArgb;
                        else return System.Drawing.Imaging.PixelFormat.Undefined;
                    }
                default:
                    {
                        return System.Drawing.Imaging.PixelFormat.Undefined;
                    }
            }
        }



        /// <summary>
        /// Returns the number of channels related to a given 
        /// pixelformat -> <see cref="System.Drawing.Imaging.PixelFormat"/>.
        /// </summary>
        /// <param name="pixelFormat">Checked pixelformat</param>
        /// <returns>Number of color channels.</returns>
        public static unsafe int getNumOfChannels(PixelFormat pixelFormat)
        {
            int channels = 1;

            if (
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppGrayScale) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format4bppIndexed) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed)
                )
            {
                channels = 1;

            }

            else if (
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppRgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format48bppRgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppRgb555) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppRgb565)
            )
            {
                channels = 3;

            }
            else if (
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppArgb1555) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format64bppArgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format64bppPArgb) ||
                (pixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppPArgb)
                )
            {
                channels = 4;
            }


            return channels;
        }




        /// <summary>
        /// Returns the bit depth of the bitmap related to a given 
        /// pixelformat -> <see cref="System.Drawing.Imaging.PixelFormat"/>.
        /// </summary>
        /// <param name="pixelFormat">Checked pixelformat.</param>
        /// <returns>Number of color channels.</returns>
        public static int getBitDepth(PixelFormat pixelFormat)
        {
            int depth = 8;

            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    {
                        depth = 16;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    {
                        depth = 8;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
                    {
                        depth = 4;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    {
                        depth = 8;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    {
                        depth = 8;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
                    {
                        depth = 8;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
                    {
                        depth = 16;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
                    {
                        depth = 16;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
                    {
                        depth = 16;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
                    {
                        depth = 5;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
                    {
                        depth = 5;
                        break;
                    }

            }

            return depth;
        }




        /// <summary>
        /// Returns the byte depth (number of bytes per channel) of 
        /// the bitmap related to a given pixelformat -> <see cref="System.Drawing.Imaging.PixelFormat"/>.
        /// </summary>
        /// <param name="pixelFormat">Checked pixelformat.</param>
        /// <returns>Number of color channels.</returns>
        public static int getByteDepth(PixelFormat pixelFormat)
        {
            int depth = 1;

            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    {
                        depth = 2;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
                    {
                        depth = 2;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
                    {
                        depth = 2;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
                    {
                        depth = 2;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
                    {
                        depth = 1;
                        break;
                    }
                case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
                    {
                        depth = 1;
                        break;
                    }
                default:
                    {
                        depth = 1;
                        break;
                    }
            }
            return depth;
        }



        /// <summary>
        /// Gets the stride.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="byteDepth">The byte depth.</param>
        /// <returns></returns>
        public static int getStride(int width, int byteDepth)
        {
            int temp = (width * byteDepth);
            return temp + (4 - (temp%4));
        }



        /// <summary>
        /// Static method that determines the best fit of a
        /// given bitmap within a target rectangle.
        /// </summary>
        /// <param name="bmp">The <see cref="System.Drawing.Bitmap" />.</param>
        /// <param name="targetArea">The target area.</param>
        /// <returns>
        ///   <see cref="System.Drawing.Rectangle" /> that corresponds to the new image size.
        /// </returns>
        public static System.Drawing.Rectangle ScaleToFit(Bitmap bmp, Rectangle targetArea)
        {
            Rectangle result = new Rectangle(targetArea.Location, targetArea.Size);

            //Within this method, determine
            //whether the bitmap fits best
            //along the width or the height. If
            //width, then adjust the result to
            //fit to the width.
            // Determine best fit: width or height
            if (result.Height * bmp.Width > result.Width * bmp.Height) // the same as: if (result.Height/result.Width >  bmp.Height/bmp.Width)
            {
                // Final width should match target,
                // determine and center height
                result.Height = result.Width * bmp.Height / bmp.Width;
                result.Y += (targetArea.Height - result.Height) / 2;
            }
            //If the bitmap fits best along the
            //height, adjust the result to fit to
            //the height.
            else
            {
                // Final height should match target,
                // determine and center width
                result.Width = result.Height * bmp.Width / bmp.Height;
                result.X += (targetArea.Width - result.Width) / 2;
            }
            //Either way, return the resulting
            //rectangle.
            return result;
        }




        /// <summary>
        /// Method that determines the best fit of a
        /// given <see cref="System.Drawing.Rectangle" /> within a target rectangle.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="targetArea">The target area.</param>
        /// <returns>
        ///   <see cref="System.Drawing.Rectangle" /> that corresponds to the new image size.
        /// </returns>
        public static System.Drawing.Rectangle ScaleToFit(Rectangle rectangle, Rectangle targetArea)
        {
            float scaleFactor = 1;
            return ScaleToFit(rectangle, targetArea, out scaleFactor);
        }



        /// <summary>
        /// Method that determines the best fit of a
        /// given <see cref="System.Drawing.Rectangle" /> within a target rectangle.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="targetArea">The target area.</param>
        /// <returns>
        ///   <see cref="System.Drawing.Rectangle" /> that corresponds to the new image size.
        /// </returns>
        public static System.Drawing.RectangleF ScaleToFit(RectangleF rectangle, RectangleF targetArea)
        {
            float scaleFactor = 1;
            return ScaleToFit(rectangle, targetArea, out scaleFactor);
        }



        /// <summary>
        /// Method that determines the best fit of a
        /// given <see cref="System.Drawing.Rectangle" /> within a target rectangle.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="targetArea">The target area.</param>
        /// <param name="scaleFactor">The scale factor.</param>
        /// <returns>
        ///   <see cref="System.Drawing.Rectangle" /> that corresponds to the new image size.
        /// </returns>
        public static System.Drawing.Rectangle ScaleToFit(RectangleF rectangle, Rectangle targetArea, out float scaleFactor)
        {

            Rectangle result = new Rectangle(targetArea.Location, targetArea.Size);


            scaleFactor = 1;
            //Within this method, determine
            //whether the bitmap fits best
            //along the width or the height. If
            //width, then adjust the result to
            //fit to the width.
            // Determine best fit: width or height
            if (result.Height * rectangle.Width > result.Width * rectangle.Height) // the same as: if (result.Height/result.Width >  bmp.Height/bmp.Width)
            {
                // Final width should match target,
                // determine and center height
                scaleFactor = (float)rectangle.Height / (float)rectangle.Width;

                result.Height = (int)Math.Round(result.Width * scaleFactor);
                result.Y += (int)Math.Round((float)(targetArea.Height - result.Height) / 2.0f);
            }
            //If the bitmap fits best along the
            //height, adjust the result to fit to
            //the height.
            else
            {
                // Final height should match target,
                // determine and center width
                scaleFactor = (float)rectangle.Width / (float)rectangle.Height;

                result.Width = (int)Math.Round(result.Height * scaleFactor);
                result.X += (int)Math.Round((float)(targetArea.Width - result.Width) / 2.0f);
            }
            //Either way, return the resulting
            //rectangle.
            return result;
        }




        /// <summary>
        /// Method that determines the best fit of a
        /// given <see cref="System.Drawing.Rectangle" /> within a target rectangle.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="targetArea">The target area.</param>
        /// <param name="scaleFactor">The scale factor.</param>
        /// <returns>
        ///   <see cref="System.Drawing.Rectangle" /> that corresponds to the new image size.
        /// </returns>
        public static System.Drawing.RectangleF ScaleToFit(RectangleF rectangle, RectangleF targetArea, out float scaleFactor)
        {

            RectangleF result = new RectangleF(targetArea.Location, targetArea.Size);

            scaleFactor = 1;
            //Within this method, determine
            //whether the bitmap fits best
            //along the width or the height. If
            //width, then adjust the result to
            //fit to the width.
            // Determine best fit: width or height
            if (result.Height * rectangle.Width > result.Width * rectangle.Height) // the same as: if (result.Height/result.Width >  bmp.Height/bmp.Width)
            {
                // Final width should match target,
                // determine and center height
                scaleFactor = (float)rectangle.Height / (float)rectangle.Width;

                result.Height = (result.Width * scaleFactor);
                result.Y += ((float)(targetArea.Height - result.Height) / 2.0f);
            }
            //If the bitmap fits best along the
            //height, adjust the result to fit to
            //the height.
            else
            {
                // Final height should match target,
                // determine and center width
                scaleFactor = (float)rectangle.Width / (float)rectangle.Height;

                result.Width = (result.Height * scaleFactor);
                result.X += ((float)(targetArea.Width - result.Width) / 2.0f);
            }
            //Either way, return the resulting
            //rectangle.
            return result;
        }




        /// <summary>
        /// Method that returns a rectangle that fits best into a viewport and represents.
        /// </summary>
        /// <param name="ImageSize">The size of an image or rectangle.</param>
        /// <param name="DisplaySize">The size of the viewport.</param>
        /// <returns>
        /// The <see cref="System.Drawing.Rectangle" /> which fits best into the viewport.
        /// </returns>
        public static System.Drawing.Rectangle ScaleToFit(System.Drawing.Size ImageSize, System.Drawing.Size DisplaySize)
        {
            System.Drawing.Rectangle TempRect = new Rectangle();

            if ((((float)ImageSize.Width) / ((float)ImageSize.Height)) >= (((float)DisplaySize.Width) / ((float)DisplaySize.Height)))
            {

                float tempFactor = ((float)DisplaySize.Width / (float)ImageSize.Width);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.Y = (int)Math.Round((((float)(DisplaySize.Height) / (2.0f)) - ((float)(TempRect.Height) / (2.0f))));

            }
            //if ((ImageSize.Width / ImageSize.Height) < (DisplaySize.Width / DisplaySize.Height))
            else
            {

                float tempFactor = ((float)DisplaySize.Height / (float)ImageSize.Height);

                TempRect.Width = (int)Math.Round((tempFactor * (float)ImageSize.Width));
                TempRect.Height = (int)Math.Round((tempFactor * (float)ImageSize.Height));
                TempRect.X = (int)Math.Round(((DisplaySize.Width) / (2.0f)) - ((float)(TempRect.Width) / (2.0f)));

            }
            return TempRect;
        }




        /// <summary>
        /// Method for resizing 24 and 32 bit images.
        /// </summary>
        /// <param name="inBMP"> The source <see cref="System.Drawing.Bitmap"/>.</param>
        /// <param name="widthBMP">The resized image width.</param>
        /// <param name="heightBMP">The resized image height.</param>
        /// <param name="IntPMode">The interpolationmode.</param>
        /// <returns>The resized Image as a bitmap object.</returns>
        public static System.Drawing.Bitmap scaleBitmap(System.Drawing.Bitmap inBMP,
            int widthBMP, int heightBMP,
            System.Drawing.Drawing2D.InterpolationMode IntPMode)
        {

            try
            {
                System.Drawing.Bitmap outBMP = new System.Drawing.Bitmap(widthBMP, heightBMP);

                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(inBMP))
                {
                    g.InterpolationMode = IntPMode;

                    g.DrawImage(inBMP, new System.Drawing.Rectangle(0, 0, widthBMP, heightBMP),
                        new System.Drawing.Rectangle(0, 0, inBMP.Width, inBMP.Height), System.Drawing.GraphicsUnit.Pixel);

                }

                return outBMP;

            }
            finally
            {

            }

        }



        /// <summary>
        /// Scales the <paramref name="inputBitmap"/> to the new image width and height with no interpolation.
        /// </summary>
        /// <param name="inputBitmap">The input bitmap.</param>
        /// <param name="newWidth">The new width.</param>
        /// <param name="newHeight">The new height.</param>
        /// <returns>The scaled Bitmap</returns>
        public unsafe static System.Drawing.Bitmap scaleBitmap(System.Drawing.Bitmap inputBitmap, int newWidth, int newHeight)
        {

            System.Drawing.Bitmap outputBitmap = new System.Drawing.Bitmap(newWidth, newHeight, inputBitmap.PixelFormat);


            float scaleFactorW = ((float)newWidth) / ((float)inputBitmap.Width);
            float specScaleFW = scaleFactorW * 3.0f;
            float scaleFactorH = ((float)newHeight) / ((float)inputBitmap.Height);


            System.Drawing.Rectangle myImgRectOut, myImgRectIn;

            myImgRectIn = new System.Drawing.Rectangle(0, 0, inputBitmap.Width, inputBitmap.Height);
            myImgRectOut = new System.Drawing.Rectangle(0, 0, outputBitmap.Width, outputBitmap.Height);

            System.Drawing.Imaging.BitmapData bmpDataIn = inputBitmap.LockBits(myImgRectIn, ImageLockMode.ReadWrite, inputBitmap.PixelFormat);
            System.Drawing.Imaging.BitmapData bmpDataOut = outputBitmap.LockBits(myImgRectOut, ImageLockMode.ReadWrite, outputBitmap.PixelFormat);


            int CounterOut = 0;
            int* ptrCounter = &CounterOut;
            int CounterIn = 0;
            int* ptrCounterIn = &CounterIn;

            int startX = 0;
            int startY = 0;
            int bmpWidthPixelsIn = bmpDataIn.Width * 4;
            int bmpWidthPixelsOut = bmpDataOut.Width * 4;
            int width = bmpDataOut.Width;
            int height = bmpDataOut.Height;
            int* ptrWidth = &width,
                ptrHeight = &height,
                ptrStartX = &startX,
                ptrStartY = &startY,
                ptrBmpWidthPixelsOut = &bmpWidthPixelsOut,
                ptrBmpWidthPixelsIn = &bmpWidthPixelsIn;

            try
            {

                switch (inputBitmap.PixelFormat)
                {

                    case (PixelFormat.Format32bppArgb):
                        {

                            int u = 0, v = 0;

                            {


                                byte* ptrIn = (byte*)bmpDataIn.Scan0;
                                byte* ptrOut = (byte*)bmpDataOut.Scan0;


                                for (int y = *ptrStartY; y < (*ptrHeight); y++)
                                {

                                    // Inverse scale Matrix !! =>
                                    // y-coord. for the inImage - nearest neighbour
                                    v = (int)System.Math.Round(((float)y) / scaleFactorH);

                                    for (int x = *ptrStartX; x < (*ptrWidth); x++)
                                    {
                                        // Inverse scale Matrix !! =>
                                        // x, y outImage gets values from Pixel u,v from inImage
                                        // Pointer to inImage must be set to the right position
                                        // which is v
                                        // v = (int)System.Math.Round( ((float)y)/scaleFactorH) )
                                        // but more difficult is the calculation of the x-coordinate of the input image
                                        // 
                                        // u = (int)System.Math.Round( ((float)x)/specScaleFW ) * 3;


                                        u = (int)System.Math.Round(((float)x) / scaleFactorW);


                                        *ptrCounter = y * (*ptrBmpWidthPixelsOut) + x * 4;
                                        *ptrCounterIn = v * (*ptrBmpWidthPixelsIn) + u * 4;


                                        // blue
                                        ptrOut[*ptrCounter + 0] = (byte)(ptrIn[*ptrCounterIn + 0]);
                                        // green
                                        ptrOut[*ptrCounter + 1] = (byte)(ptrIn[*ptrCounterIn + 1]);
                                        // red
                                        ptrOut[*ptrCounter + 2] = (byte)(ptrIn[*ptrCounterIn + 2]);

                                        //// alpha-channel
                                        ptrOut[*ptrCounter + 3] = 255;
                                    }

                                }
                            }
                           

                            break;
                        }
                    case (PixelFormat.Format16bppGrayScale):
                        {

                           

                            break;
                        }
                    case (PixelFormat.Format64bppArgb):
                        {

                           

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }


                if (bmpDataIn != null)
                {
                    inputBitmap.UnlockBits(bmpDataIn);
                    bmpDataIn = null;
                }

                if (bmpDataOut != null)
                {
                    outputBitmap.UnlockBits(bmpDataOut);
                    bmpDataOut = null;
                }


            }
            catch (Exception ex)
            {
                 
            }
            finally
            {
                if (bmpDataIn != null)
                {
                    inputBitmap.UnlockBits(bmpDataIn);
                    bmpDataIn = null;
                }

                if (bmpDataOut != null)
                {
                    outputBitmap.UnlockBits(bmpDataOut);
                    bmpDataOut = null;
                }
            }

            return outputBitmap;
        }





    }
}
